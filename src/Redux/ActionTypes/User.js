// Author = Sina

const userActionTypes = {
  SET_USERID: 'SET_USERID',
  REMOVE_PROFILE: 'REMOVE_PROFILE',
  SET_PROFILE_INFO : 'SET_PROFILE_INFO'
}

export default userActionTypes
