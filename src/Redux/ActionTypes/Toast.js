// Author = MJS

const ToastActionTypes = {
  SHOW_TOAST : 'SHOW_TOAST',
  HIDE_TOAST : 'HIDE_TOAST',
  TOGGLE_SIDEBAR : 'TOGGLE_SIDEBAR',
}

export default ToastActionTypes
