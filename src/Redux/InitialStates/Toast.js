// Author = MJS

const toastInitialState = {
  visible : false,
  content : '',
  sidebar  : false,
}

export default toastInitialState
