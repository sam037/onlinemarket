// Author: Sina

import AuthInitialState from './Authentication'
import ProductInitialState from './Product'
import UserInitialState from './User'
import MenuInitialState from './Menu'
import ToastInitialState from './Toast'

export {
  AuthInitialState,
  ProductInitialState,
  UserInitialState,
  MenuInitialState,
  ToastInitialState,
}
