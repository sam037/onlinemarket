import {
  UserActionTypes
} from '../ActionTypes'
import {
  UserInitialState
} from '../InitialStates'

const UserReducer = (state = UserInitialState, action) => {
  const { type, payload } = action
  switch (type) {
    case UserActionTypes.SET_PROFILE_INFO:
      return {
        ...state,
        profile: {
          email               : payload.info.email,
          ballance            : payload.info.ballance,
          count               : payload.info.count,
          role                : payload.info.roles,
        }
      }
    case UserActionTypes.SET_USERID:
      return {
        ...state,
        user_id : payload.id,
      }
    case UserActionTypes.REMOVE_PROFILE:
      return {
        ...state,
        user_id               : null,
        profile: {
          email               : null,
          ballance            : null,
          count               : null,
          role                : null,
        }
      }
    default:
      return { ...state }
  }
}

export default UserReducer
