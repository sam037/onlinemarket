// Author = MJ

import {
  MenuActionTypes
} from '../ActionTypes'
import {
  MenuInitialState
} from '../InitialStates'

const MenuReducer = (state = MenuInitialState, action) => {
  const { type } = action
  switch (type) {
    case MenuActionTypes.TOGGLE_MENU:
      return {
        ...state,
        status    : !state.status,
      }
    default:
      return { ...state }
  }
}

export default MenuReducer
