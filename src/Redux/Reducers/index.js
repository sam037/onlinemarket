// Author: Sina

import {
  combineReducers
} from 'redux'
import {
  persistReducer
} from 'redux-persist'

import AuthenticationReducer from './Authentication'
import ToastReducer from './Toast'
import UserReducer from './User'
import storage from 'redux-persist/es/storage'
import autoMergeLevel2 from  'redux-persist/lib/stateReconciler/autoMergeLevel2'

const rootPersistConfig = {
	key: 'root',
	storage: storage,
	stateReconciler: autoMergeLevel2,
}

const authPersistConfig = {
	key: 'auth',
	storage: storage,
	stateReconciler: autoMergeLevel2,
}

const userPersistConfig = {
	key: 'user',
	storage: storage,
	stateReconciler: autoMergeLevel2,
}

const rootReducer = combineReducers({
  Authentication: AuthenticationReducer,
  User          : UserReducer,
  Toast         : ToastReducer,
})

export default persistReducer (rootPersistConfig, rootReducer);
