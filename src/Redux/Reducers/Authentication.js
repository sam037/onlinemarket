// Author = Sina

import {
  AuthActionTypes
} from '../ActionTypes'
import {
  AuthInitialState
} from '../InitialStates'

const AuthenticationReducer = (state = AuthInitialState, action) => {
  const { type, payload } = action
  switch (type) {
    case AuthActionTypes.SET_CREDENTIALS:
      return {
        ...state,
        access_key  : payload.access_key,
        roles       : payload.roles,
        user_id: payload.user_id
      }
    case AuthActionTypes.SET_CREDENTIALS_ACCESSKEY:
      return {
        ...state,
        access_key  : payload.access_key,
      }
    case AuthActionTypes.REMOVE_CREDENTIALS:
      return {
        ...state,
        access_key: null,
        user_id   : null,
        roles     : null,
      }
    default:
      return { ...state }
  }
}

export default AuthenticationReducer
