// Author = MJ

import {
  ToastActionTypes
} from '../ActionTypes'
import {
  ToastInitialState
} from '../InitialStates'

const ToastReducer = (state = ToastInitialState, action) => {
  const { type } = action
  switch (type) {
    case ToastActionTypes.SHOW_TOAST:
      return {
        ...state,
        visible   : true,
        content   : action.payload
      }
    case ToastActionTypes.HIDE_TOAST:
      return {
        ...state,
        visible   : false,
        content   : null
      }
    case ToastActionTypes.TOGGLE_SIDEBAR:
      let sidebarstate = !state.sidebar
      return {
        ...state,
        sidebar   : sidebarstate,
      }
    default:
      return { ...state }
  }
}

export default ToastReducer
