// Author: MJS
//import md5 from 'blueimp-md5'
import {
  AuthActionTypes,
} from '../ActionTypes'

import {
  CONSTANTS,
  RequestBuilder,
  Txt
} from '../../Utils'
import ToastActions from './Toast'
import { post } from 'axios';

const AuthActions = {
  /* Check parameter validity */
  CheckUser: (user_name) => async (dispatch, getState) => {
    const request = new RequestBuilder()
                          .setUrl(`${CONSTANTS.BASE_API_URL}/${CONSTANTS.AUTHENTICATION.CHECK_USER}`)
                          .setMethod('GET')
                          .addHeader('Content-Type', 'application/json')
                          .addHeader('x-user-info', user_name)
                          .getRequest()
    let responseObject = {}
    try {
      const response = await fetch(request)
      switch (response.status) {
        case 204:
          responseObject = { ...responseObject, error : false }
          break;
        case 409:
          responseObject = { ...responseObject, error : true }
          break;
        default:
          dispatch(ToastActions.ShowToast(Txt.PleaseTryAgain))
          responseObject = { ...responseObject, payload: { message: CONSTANTS.ERROR_MESSAGE.E_500 }, error: true }
          break;
      }
    } catch (e) {
      dispatch(ToastActions.ShowToast(Txt.CantConnect))
      responseObject = { ...responseObject, payload: { message: e.message }, error: true }
    }

    return responseObject
  },
  CheckPhone: (phone) => async (dispatch, getState) => {
    const request = new RequestBuilder()
                          .setUrl(`${CONSTANTS.BASE_API_URL}/${CONSTANTS.AUTHENTICATION.CHECK_PHONE}`)
                          .setMethod('GET')
                          .addHeader('Content-Type', 'application/json')
                          .addHeader('x-user-info', phone)
                          .getRequest()
    let responseObject = {}
    try {
      const response = await fetch(request)
      switch (response.status) {
        case 204:
          responseObject = { ...responseObject, error : false }
          break;
        case 409:
          responseObject = { ...responseObject, error : true }
          break;
        default:
          dispatch(ToastActions.ShowToast(Txt.PleaseTryAgain))
          responseObject = { ...responseObject, payload: { message: CONSTANTS.ERROR_MESSAGE.E_500 }, error: true }
          break;
      }
    } catch (e) {
      dispatch(ToastActions.ShowToast(Txt.CantConnect))
      responseObject = { ...responseObject, payload: { message: e.message }, error: true }
    }

    return responseObject
  },
  CheckEmail: (email) => async (dispatch, getState) => {
    const request = new RequestBuilder()
                          .setUrl(`${CONSTANTS.BASE_API_URL}/${CONSTANTS.AUTHENTICATION.CHECK_EMAIL}`)
                          .setMethod('GET')
                          .addHeader('Content-Type', 'application/json')
                          .addHeader('x-user-info', email)
                          .getRequest()
    let responseObject = {}
    try {
      const response = await fetch(request)
      switch (response.status) {
        case 204:
          responseObject = { ...responseObject, error : false }
          break;
        case 409:
          responseObject = { ...responseObject, error : true }
          break;
        default:
          dispatch(ToastActions.ShowToast(Txt.PleaseTryAgain))
          responseObject = { ...responseObject, payload: { message: CONSTANTS.ERROR_MESSAGE.E_500 }, error: true }
          break;
      }
    } catch (e) {
      dispatch(ToastActions.ShowToast(Txt.CantConnect))
      responseObject = { ...responseObject, payload: { message: e.message }, error: true }
    }

    return responseObject
  },
  /* refferal actions */
  SetRefferal: (refferal) => async (dispatch, getState) => {
    dispatch({
      type : AuthActionTypes.SET_REFFERAL,
      payload : {refferal}
    })
  },
  DelRefferal: (refferal) => async (dispatch, getState) => {
    dispatch({
      type : AuthActionTypes.DEL_REFFERAL,
    })
  },
  /* api actions */
  signIn: (input, type, raw_password) => async (dispatch, getState) => {
    //let password = md5(raw_password ,"StartApp")
    let password = raw_password
    const request = new RequestBuilder()
                          .setUrl(`${CONSTANTS.BASE_API_URL}/${CONSTANTS.AUTHENTICATION.SIGN_IN}`)
                          .setMethod('POST')
                          .addHeader('Content-Type', 'application/json')
                          .setBody({ input, type, password })
                          .getRequest()

    let responseObject = {};
    try {
      const response = await fetch(request)
      switch (response.status) {
        case 200:
          const json = await response.json()
          dispatch({
            type: AuthActionTypes.SET_CREDENTIALS,
            payload: { access_key: response.headers.get('x-access-key'),
                      roles: response.headers.get('roles'),
                      user_id: json.id }
          })
          console.log({ access_key: response.headers.get('x-access-key'),
          roles: response.headers.get('roles'),
          user_id: json.id })
          dispatch(ToastActions.ShowToast(Txt.SuccessFullLogin))
          break;
        case 400:
          dispatch(ToastActions.ShowToast(Txt.CantConnect))
          responseObject = { ...responseObject, payload: { message: CONSTANTS.ERROR_MESSAGE.E_400 }, error: true }
          break;
        case 404:
          dispatch(ToastActions.ShowToast(Txt.UserOrPassNot))
          responseObject = { ...responseObject, payload: { message: CONSTANTS.ERROR_MESSAGE.E_404.USER}, error: true }
          break;
        default:
          dispatch(ToastActions.ShowToast(Txt.PleaseTryAgain))
          responseObject = { ...responseObject, payload: { message: CONSTANTS.ERROR_MESSAGE.E_500 }, error: true }
      }
    } catch (e) {
      dispatch(ToastActions.ShowToast(Txt.CantConnect))
      responseObject = { ...responseObject, payload: { message: e.message }, error: true }
    }

    return responseObject
  },
  signUp: (username, phone, raw_password) => async (dispatch, getState) => {
    //let password = md5(raw_password , "StartApp")
    let password = raw_password
    let referral = getState().Authentication.refferal

    const request = new RequestBuilder()
                          .setUrl(`${CONSTANTS.BASE_API_URL}/${CONSTANTS.AUTHENTICATION.SIGN_UP}`)
                          .setMethod('POST')
                          .addHeader('Content-Type', 'application/json')
                          .setBody({ username, phone, password, referral })
                          .getRequest()

    let responseObject = {}
    try {
      const response = await fetch(request)
      switch (response.status) {
        case 204:
          responseObject = { ...responseObject, payload: { user_info: response.headers.get('x-user-info') } }
          break;
        case 409:
          responseObject = { ...responseObject, error: true }
          dispatch(ToastActions.ShowToast(Txt.Conflicts))
          break;
        case 404:
          responseObject = { ...responseObject, error: true ,redirect : true}
          dispatch({ type : AuthActionTypes.DEL_REFFERAL })
          dispatch(ToastActions.ShowToast(Txt.ReffrealNotValid))
          break;
        default:
          dispatch(ToastActions.ShowToast(Txt.PleaseTryAgain))
          responseObject = { ...responseObject, payload: { message: CONSTANTS.ERROR_MESSAGE.E_500 }, error: true }
      }
    } catch (e) {
      dispatch(ToastActions.ShowToast(Txt.CantConnect))
      responseObject = { ...responseObject, payload: { message: e.message }, error: true }
    }

    return responseObject
  },
  verifyCode: (user_info, verification_code) => async (dispatch, getState) => {
    const request = new RequestBuilder()
                          .setUrl(`${CONSTANTS.BASE_API_URL}/${CONSTANTS.AUTHENTICATION.VERIFY_PHONE_CODE}`)
                          .setMethod('POST')
                          .addHeader('Content-Type', 'application/json')
                          .addHeader('x-user-info', user_info)
                          .setBody({ code : verification_code })
                          .getRequest()
    let responseObject = {}
    try {
      const response = await fetch(request)
      switch (response.status) {
        case 201:
          responseObject = { ...responseObject, payload: { user_info: response.headers.get('x-user-info') } }
          break;
        case 403:
          dispatch(ToastActions.ShowToast(Txt.CodeExpired))
          responseObject = { ...responseObject, payload: { message: CONSTANTS.ERROR_MESSAGE.E_401 }, error: true }
          break;
        case 406:
          dispatch(ToastActions.ShowToast(Txt.CodeNotValid))
          responseObject = { ...responseObject, payload: { message: CONSTANTS.ERROR_MESSAGE.E_406 }, error: true }
          break;
        default:
          dispatch(ToastActions.ShowToast(Txt.PleaseTryAgain))
          responseObject = { ...responseObject, payload: { message: CONSTANTS.ERROR_MESSAGE.E_500 }, error: true }
          break;
      }
    } catch (e) {
      dispatch(ToastActions.ShowToast(Txt.CantConnect))
      responseObject = { ...responseObject, payload: { message: e.message }, error: true }
    }

    return responseObject
  },
  resendCode: (user_info) => async (dispatch, getState) => {
    const request = new RequestBuilder()
                          .setUrl(`${CONSTANTS.BASE_API_URL}/${CONSTANTS.AUTHENTICATION.RESEND_CODE}`)
                          .setMethod('POST')
                          .addHeader('Content-Type', 'application/json')
                          .addHeader('x-user-info', user_info)
                          .getRequest()

    let responseObject = {}

    try {
      const response = await fetch(request)

      switch (response.status) {
        case 204:
          responseObject = { ...responseObject, payload: { user_info: response.headers.get('x-user-info') } }
          break;
        default:
          dispatch(ToastActions.ShowToast(Txt.PleaseTryAgain))
          responseObject = { ...responseObject, payload: { message: CONSTANTS.ERROR_MESSAGE.E_500 }, error: true }
          break;
      }
    } catch (e) {
      dispatch(ToastActions.ShowToast(Txt.CantConnect))
      responseObject = { ...responseObject, payload: { message: e.message }, error: true }
    }

    return responseObject
  },
  changePhone: (user_info, new_phone_number) => async (dispatch, getState) => {
    const request = new RequestBuilder()
                          .setUrl(`${CONSTANTS.BASE_API_URL}/${CONSTANTS.AUTHENTICATION.CHANGE_PHONE}`)
                          .setMethod('PUT')
                          .addHeader('Content-Type', 'application/json')
                          .addHeader('x-user-info', user_info)
                          .setBody({ phone : new_phone_number })
                          .getRequest()

    let responseObject = {}

    try {
      const response = await fetch(request)

      switch (response.status) {
        case 204:
          responseObject = { ...responseObject, payload: { user_info: response.headers.get('x-user-info') } }
          break;
        case 401:
          responseObject = { ...responseObject, payload: { message: CONSTANTS.ERROR_MESSAGE.E_401 }, error: true }
          break;
        default:
          dispatch(ToastActions.ShowToast(Txt.PleaseTryAgain))
          responseObject = { ...responseObject, payload: { message: CONSTANTS.ERROR_MESSAGE.E_500 }, error: true }
      }
    } catch (e) {
      dispatch(ToastActions.ShowToast(Txt.CantConnect))
      responseObject = { ...responseObject, payload: { message: e.message }, error: true }
    }

    return responseObject
  },
  resetPassword: (type, input) => async (dispatch, getState) => {
    const request = new RequestBuilder()
        .setUrl(`${CONSTANTS.BASE_API_URL}/${CONSTANTS.AUTHENTICATION.RESET_PASSWORD}`)
        .setMethod('POST')
        .addHeader('Content-Type', 'application/json')
        .setBody({ type, input })
        .getRequest()

    let responseObject = {}

    try {
      const response = await fetch(request)

      switch (response.status) {
        case 204:
          responseObject = { ...responseObject, payload: {}, error: false }
          break;
        case 404:
          dispatch(ToastActions.ShowToast(Txt.UserNotExist))
          responseObject = { ...responseObject, payload: { message: CONSTANTS.ERROR_MESSAGE.E_404.USER }, error: true }
          break;
        default:
          dispatch(ToastActions.ShowToast(Txt.PleaseTryAgain))
          responseObject = { ...responseObject, payload: { message: CONSTANTS.ERROR_MESSAGE.E_500 }, error: true }
          break;
      }
    } catch (e) {
      dispatch(ToastActions.ShowToast(Txt.CantConnect))
      responseObject = { ...responseObject, payload: { message: e.message }, error: true }
    }

    return responseObject
  },
  SubmitNewPassword : (validToken , newPassword) => async (dispatch , getState) => {
    const request = new RequestBuilder()
                          .setUrl(`${CONSTANTS.BASE_API_URL}/${CONSTANTS.AUTHENTICATION.SUBMIT_NEW_PASS}`)
                          .setMethod('PUT')
                          .addHeader('Content-Type', 'application/json')
                          .setBody({token : validToken , password : newPassword })
                          .getRequest()

    let responseObject = {}
    try {
      const response = await fetch(request)
      switch (response.status) {
        case 204:
          responseObject = { ...responseObject, payload: {}, error: false }
          break;
        default:
          dispatch(ToastActions.ShowToast(Txt.PleaseTryAgain))
          responseObject = { ...responseObject, payload: { message: CONSTANTS.ERROR_MESSAGE.E_500 }, error: true }
          break;
      }
    } catch (e) {
      dispatch(ToastActions.ShowToast(Txt.CantConnect))
      responseObject = { ...responseObject, payload: { message: e.message }, error: true }
    }

    return responseObject
  },
  CheckValidityTocken : (tocken) => async(dispatch,getState) =>{
    const request = new RequestBuilder()
                          .setUrl(`${CONSTANTS.BASE_API_URL}/${CONSTANTS.AUTHENTICATION.VERIFY_RESET_PASS}`)
                          .setMethod('GET')
                          .addHeader('Content-Type', 'application/json')
                          .addHeader('x-user-info', tocken)
                          .getRequest()

    let responseObject = {}
    try {
      const response = await fetch(request)
      console.log(response.status)
      switch (response.status) {
        case 204:
          responseObject = { ...responseObject, payload: {}, error: false }
          break;
        default:
          dispatch(ToastActions.ShowToast(Txt.PleaseTryAgain))
          responseObject = { ...responseObject, payload: { message: CONSTANTS.ERROR_MESSAGE.E_500 }, error: true }
          break;
      }
    } catch (e) {
      dispatch(ToastActions.ShowToast(Txt.CantConnect))
      responseObject = { ...responseObject, payload: { message: e.message }, error: true }
    }

    return responseObject
  },
  SignOut: () => async (dispatch, getState) => {
    let access_key = getState().Authentication.access_key
    const request = new RequestBuilder()
                          .setUrl(`${CONSTANTS.BASE_API_URL}/${CONSTANTS.USER.SIGN_OUT}`)
                          .setMethod('POST')
                          .addHeader('Content-Type', 'application/json')
                          .addHeader('x-access-key', access_key)
                          .getRequest()
    let responseObject = {}
    try {
      const response = await fetch(request)
      switch (response.status) {
        case 201:
          dispatch(ToastActions.ShowToast(Txt.SignedOut))
          dispatch({ type: AuthActionTypes.REMOVE_CREDENTIALS })
          break;
        default:
          dispatch(ToastActions.ShowToast(Txt.SignedOut))
          dispatch({ type: AuthActionTypes.REMOVE_CREDENTIALS })
          responseObject = { ...responseObject, payload: { message: CONSTANTS.ERROR_MESSAGE.E_500 }, error: true }
          break;
      }
    } catch (e) {
      dispatch(ToastActions.ShowToast(Txt.CantConnect))
      dispatch({ type: AuthActionTypes.REMOVE_CREDENTIALS })
      responseObject = { ...responseObject, payload: { message: e.message }, error: true }
    }
    return responseObject
  },
}

export default AuthActions
/*
  CheckRNN: (nNational) => async (dispatch, getState) => {
    const request = new RequestBuilder()
                          .setUrl(`${CONSTANTS.BASE_API_URL}/${CONSTANTS.AUTHENTICATION.CHECK_RNN}`)
                          .setMethod('GET')
                          .addHeader('Content-Type', 'application/json')
                          .addHeader('x-user-info', nNational)
                          .getRequest()
    let responseObject = {}
    try {
      const response = await fetch(request)
      switch (response.status) {
        case 204:
          responseObject = { ...responseObject, error : false }
          break;
        case 409:
          responseObject = { ...responseObject, error : true }
          break;
        default:
          dispatch(ToastActions.ShowToast(Txt.PleaseTryAgain))
          responseObject = { ...responseObject, payload: { message: CONSTANTS.ERROR_MESSAGE.E_500 }, error: true }
          break;
      }
    } catch (e) {
      dispatch(ToastActions.ShowToast(Txt.CantConnect))
      responseObject = { ...responseObject, payload: { message: e.message }, error: true }
    }

    return responseObject
  },
  CheckLNN: (nNational) => async (dispatch, getState) => {
    const request = new RequestBuilder()
                          .setUrl(`${CONSTANTS.BASE_API_URL}/${CONSTANTS.AUTHENTICATION.CHECK_LNN}`)
                          .setMethod('GET')
                          .addHeader('Content-Type', 'application/json')
                          .addHeader('x-user-info', nNational)
                          .getRequest()
    let responseObject = {}
    try {
      const response = await fetch(request)
      switch (response.status) {
        case 204:
          responseObject = { ...responseObject, error : false }
          break;
        case 409:
          responseObject = { ...responseObject, error : true }
          break;
        default:
          dispatch(ToastActions.ShowToast(Txt.PleaseTryAgain))
          responseObject = { ...responseObject, payload: { message: CONSTANTS.ERROR_MESSAGE.E_500 }, error: true }
          break;
      }
    } catch (e) {
      dispatch(ToastActions.ShowToast(Txt.CantConnect))
      responseObject = { ...responseObject, payload: { message: e.message }, error: true }
    }

    return responseObject
  },
*/