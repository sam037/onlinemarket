//import {ProductActionType,} from '../ActionTypes'
import {
  CONSTANTS,
  RequestBuilder
} from '../../Utils'
const searchActions = {
  Basic: (keyWord) => async (dispatch, getState) => {
    const request = new RequestBuilder()
                          .setUrl(`${CONSTANTS.BASE_API_URL}/${CONSTANTS.SEARCH.BASIC}?keyword=${keyWord}`)
                          .getRequest()
    let responseObject = {};
    try {
      const response = await fetch(request)
      switch (response.status) {
        case 200:
          const product = await response.json()
          for(let i = 0 ; i < product.length ; i++) product[i].pid = product[i].id
          responseObject = { ...responseObject, payload: { product } }
          break;
        default:
          responseObject = { ...responseObject, payload: { message: CONSTANTS.ERROR_MESSAGE.E_500 }, error: true }
      }
    } catch (e) {
      responseObject = { ...responseObject, payload: { message: e.message }, error: true }
    }
    return responseObject
  },

  Advance: (keyWord) => async (dispatch, getState) => {
    const request = new RequestBuilder()
                          .setUrl(`${CONSTANTS.BASE_API_URL}/${CONSTANTS.SEARCH.ADVANCE}?${keyWord}`)
                          .getRequest()
    let responseObject = {};
    try {
      const response = await fetch(request)
      switch (response.status) {
        case 200:
          const product = await response.json()
          for(let i = 0 ; i < product.length ; i++) product[i].pid = product[i].id
          responseObject = { ...responseObject, payload: { product } }
          break;
        default:
          responseObject = { ...responseObject, payload: { message: CONSTANTS.ERROR_MESSAGE.E_500 }, error: true }
      }
    } catch (e) {
      responseObject = { ...responseObject, payload: { message: e.message }, error: true }
    }
    return responseObject
  },
}

export default searchActions
