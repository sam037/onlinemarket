//import {ProductActionType,} from '../ActionTypes'
import {
  CONSTANTS,
  RequestBuilder
} from '../../Utils'
import { post } from 'axios';

const productActions = {
  getProducts: (URL) => async (dispatch, getState) => {
    const request = new RequestBuilder()
                          .setUrl(`${CONSTANTS.BASE_API_URL}/${CONSTANTS.PRODUCT.GET_ALL_PRODUCTS}/?${URL}`)
                          .getRequest()

    let responseObject = {};
    try {
      const response = await fetch(request)

      switch (response.status) {
        case 200:
          const product = await response.json()
          responseObject = { ...responseObject, payload: { product } }
          break;
        default:
          responseObject = { ...responseObject, payload: { message: CONSTANTS.ERROR_MESSAGE.E_500 }, error: true }
      }
    } catch (e) {
      responseObject = { ...responseObject, payload: { message: e.message }, error: true }
    }

    return responseObject
  },
  getTopProducts: (URL) => async (dispatch, getState) => {
    const request = new RequestBuilder()
                          .setUrl(`${CONSTANTS.BASE_API_URL}/${CONSTANTS.PRODUCT.TOP}`)
                          .getRequest()

    let responseObject = {};
    try {
      const response = await fetch(request)

      switch (response.status) {
        case 200:
          const product = await response.json()
          responseObject = { ...responseObject, payload: { product } }
          break;
        default:
          responseObject = { ...responseObject, payload: { message: CONSTANTS.ERROR_MESSAGE.E_500 }, error: true }
      }
    } catch (e) {
      responseObject = { ...responseObject, payload: { message: e.message }, error: true }
    }

    return responseObject
  },
  cartTotal: (URL) => async (dispatch, getState) => {
    const { Authentication } = getState()
    const request = new RequestBuilder()
                          .setUrl(`${CONSTANTS.BASE_API_URL}/${CONSTANTS.PRODUCT.CARTTOT}`)
                          .addHeader('Content-Type', 'application/json')
                          .addHeader('x-access-key', Authentication.access_key)
                          .setMethod('GET')
                          .getRequest()

    let responseObject = {};
    try {
      const response = await fetch(request)

      switch (response.status) {
        case 200:
          const product = await response.json()
          responseObject = { ...responseObject, payload: { product } }
          break;
        default:
          responseObject = { ...responseObject, payload: { message: CONSTANTS.ERROR_MESSAGE.E_500 }, error: true }
      }
    } catch (e) {
      responseObject = { ...responseObject, payload: { message: e.message }, error: true }
    }

    return responseObject
  },
  buyTotal: (URL) => async (dispatch, getState) => {
    const { Authentication } = getState()
    const request = new RequestBuilder()
                          .setUrl(`${CONSTANTS.BASE_API_URL}/${CONSTANTS.PRODUCT.BUYTOTAL}`)
                          .addHeader('Content-Type', 'application/json')
                          .addHeader('x-access-key', Authentication.access_key)
                          .setMethod('GET')
                          .getRequest()

    let responseObject = {};
    try {
      const response = await fetch(request)

      switch (response.status) {
        case 200:
          const product = await response.json()
          responseObject = { ...responseObject, payload: { product } }
          break;
        default:
          responseObject = { ...responseObject, payload: { message: CONSTANTS.ERROR_MESSAGE.E_500 }, error: true }
      }
    } catch (e) {
      responseObject = { ...responseObject, payload: { message: e.message }, error: true }
    }

    return responseObject
  },
  addProduct: (title, describtion, price, category, file,thumbnail) => async (dispatch, getState) => {
    // this will return all Item wich is open for this productID
    console.log(file);
    let access_key = getState().Authentication.access_key
    let formData = new FormData();
    formData.append('title', title)
    formData.append('category', JSON.stringify(category))
    formData.append('fileToSave', file)
    formData.append('price', parseInt(price,10))
    formData.append('description', describtion)
    formData.append('thumbnail', thumbnail)

    const config = {
        headers: {'x-access-key': access_key ,
                  //'content-type': 'multipart/form-data' ,
                }
    }
    let url = `${CONSTANTS.BASE_API_URL}/${CONSTANTS.PRODUCT.ADD}`
    return post(url, formData, config)
  },
  getProductDetail: (productID) => async (dispatch, getState) => {
    // this will return all Item wich is open for this productID
    const { Authentication } = getState()
    const request = new RequestBuilder()
                          .setUrl(`${CONSTANTS.BASE_API_URL}/${CONSTANTS.PRODUCT.GET_DETAIL}/${productID}`)
                          .addHeader('Content-Type', 'application/json')
                          .addHeader('x-access-key', Authentication.access_key)
                          .getRequest()
    let responseObject = {};
    try {
      const response = await fetch(request)
      switch (response.status) {
        case 200:
          const Product = await response.json()
          responseObject = { ...responseObject, payload: { Product } }
          break;
        default:
          responseObject = { ...responseObject, payload: { message: CONSTANTS.ERROR_MESSAGE.E_500 }, error: true }
      }
    } catch (e) {
      responseObject = { ...responseObject, payload: { message: e.message }, error: true }
    }
    return responseObject
  },
  addToCart: (productId) => async (dispatch, getState) => {
    // this will return all Item wich is open for this productID
    const { Authentication } = getState()
    const request = new RequestBuilder()
                          .setUrl(`${CONSTANTS.BASE_API_URL}/${CONSTANTS.PRODUCT.ADDTOCART}`)
                          .setMethod('POST')
                          .addHeader('Content-Type', 'application/json')
                          .addHeader('x-access-key', Authentication.access_key)
                          .setBody({ productId : productId })
                          .getRequest()
    let responseObject = {};
    try {
      const response = await fetch(request)
      switch (response.status) {
        case 201:
          responseObject = { ...responseObject, error : false }
          break;
        default:
          responseObject = { ...responseObject, payload: { message: CONSTANTS.ERROR_MESSAGE.E_500 }, error: true }
      }
    } catch (e) {
      responseObject = { ...responseObject, payload: { message: e.message }, error: true }
    }
    return responseObject
  },
  payCart: (productId) => async (dispatch, getState) => {
    // this will return all Item wich is open for this productID
    const { Authentication } = getState()
    const request = new RequestBuilder()
                          .setUrl(`${CONSTANTS.BASE_API_URL}/${CONSTANTS.PRODUCT.PAYCART}`)
                          .setMethod('GET')
                          .addHeader('Content-Type', 'application/json')
                          .addHeader('x-access-key', Authentication.access_key)
                          .getRequest()
    let responseObject = {};
    try {
      const response = await fetch(request)
      console.log(response)
      switch (response.status) {
        case 200:
          responseObject = { ...responseObject, error : false }
          break;
        default:
          responseObject = { ...responseObject, payload: { message: CONSTANTS.ERROR_MESSAGE.E_500 }, error: true }
      }
    } catch (e) {
      responseObject = { ...responseObject, payload: { message: e.message }, error: true }
    }
    return responseObject
  },
  removeFromCart: (recordId) => async (dispatch, getState) => {
    // this will return all Item wich is open for this productID
    const { Authentication } = getState()
    const request = new RequestBuilder()
                          .setUrl(`${CONSTANTS.BASE_API_URL}/${CONSTANTS.PRODUCT.REMOVEFROMCART}`)
                          .setMethod('POST')
                          .addHeader('Content-Type', 'application/json')
                          .addHeader('x-access-key', Authentication.access_key)
                          .setBody({ recordId })
                          .getRequest()
    let responseObject = {};
    try {
      const response = await fetch(request)
      switch (response.status) {
        case 200:
          responseObject = { ...responseObject, error : false }
          break;
        default:
          responseObject = { ...responseObject, payload: { message: CONSTANTS.ERROR_MESSAGE.E_500 }, error: true }
      }
    } catch (e) {
      responseObject = { ...responseObject, payload: { message: e.message }, error: true }
    }
    return responseObject
  },
  requestDownload: (productId) => async (dispatch, getState) => {
    // this will return all Item wich is open for this productID
    const { Authentication } = getState()
    window.open(`${CONSTANTS.BASE_API_URL}/${CONSTANTS.PRODUCT.DOWNLOAD}/${productId}?param=${Authentication.access_key}`)
  },
}

export default productActions
