//import {UserActionTypes,} from '../ActionTypes'
import {
  CONSTANTS,
  RequestBuilder
} from '../../Utils'
import {UserActionTypes } from '../ActionTypes'
import {  Profileupdate } from '../Actions'
const UserActions = {
  GetProfileInfo: () => async (dispatch, getState) => {
    const { Authentication } = getState()
    if (!Authentication.user_id || !Authentication.access_key) return
    const request = new RequestBuilder()
                          .setUrl(`${CONSTANTS.BASE_API_URL}/${CONSTANTS.USER.PROFILE}`)
                          .setMethod('GET')
                          .addHeader('x-access-key', Authentication.access_key)
                          .getRequest()
    let responseObject = {}
    try {
      const response = await fetch(request)
      switch (response.status) {
        case 200:
          const info = await response.json()
          dispatch({ type: UserActionTypes.SET_PROFILE_INFO, payload: { info } })
          break;
        case 400:
          responseObject = { ...responseObject, payload: { message: CONSTANTS.ERROR_MESSAGE.E_400 }, error: true }
          break;
        case 404:
          responseObject = { ...responseObject, payload: { message: CONSTANTS.ERROR_MESSAGE.E_404 }, error: true }
          break;
        default:
          responseObject = { ...responseObject, payload: { message: CONSTANTS.ERROR_MESSAGE.E_500 }, error: true }
      }
    } catch (e) {
      responseObject = { ...responseObject, payload: { message: e.message }, error: true }
    }
    return responseObject
  },
}

export default UserActions
