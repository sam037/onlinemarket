import {
  ToastActionTypes,
} from '../ActionTypes'

const ToastActions = {
  ShowToast: (content) => async (dispatch, getState) => {
    dispatch({
      type : ToastActionTypes.SHOW_TOAST,
      payload : {content : content}
    })
    setTimeout(()=> dispatch({type : ToastActionTypes.HIDE_TOAST,payload : null }), 4000);
  },
}

export default ToastActions
