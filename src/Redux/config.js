// Author = MJSAM

import {
  compose,
  createStore,
  applyMiddleware,
} from 'redux'
import thunk from 'redux-thunk'
import logger from 'redux-logger'
import {
  persistStore,
} from 'redux-persist'

import rootReducer from './Reducers'

const middleware = [ thunk ]
export const loadState = () => {
  try {
    const serializedState = localStorage.getItem('state')
    if(serializedState == null){
      return undefined;
    }
    return JSON.parse(serializedState);
  } catch(e){
    return undefined;

  }
}
export const saveState = (state)=>{
  try {
    const serializedState = JSON.stringify(state)
    localStorage.setItem('state',serializedState)
  } catch(e){
    //ignore
  }
}
/*
export const configureStore = () => {
  let store = createStore(
    rootReducer,
    compose(
      applyMiddleware(...middleware, logger),
    )
  )
  let persistor = persistStore(store)

  return { store, persistor}
}
*/
//Authentication.access_key

export const configureStore = (initialState) => {
  const persistedState = loadState()
  let store = createStore(
    rootReducer,
    persistedState,
    compose(
      applyMiddleware(...middleware, logger),
    )
  )
  let persistor = persistStore(store)

  return { store,persistor}
}