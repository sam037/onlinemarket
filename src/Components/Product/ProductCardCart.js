import React,{  Component} from 'react';
import { Txt, CONSTANTS } from '../../Utils'
import ProductDetailCart from './ProductDetailCart';
//import './styles.css'
const CategoryDic = {
  'ENG' : ' فنی و مهندسی '    ,
  'HWE' : ' مهندس سخت افزار ' ,
  'SWE' : ' مهندسی نرم افزار ',
  'IT'  : ' مهندسی شبکه '     ,
  'AI'  : ' هوش مصنوعی '      ,
  }

class ProductCardCart extends Component {
  constructor(props) {
    super(props)
    this.state = {
      Detail : false,
      PostImage : this.props.Product.thumbnail ? ( `${CONSTANTS.BASE_API_URL}/images/posts/${this.props.Product.thumbnail}` ):( require('../../dist/images/postdefault.jpg') )
    }
    this.Category = ''
    try {
      this.props.Product.category.forEach(Cat => this.Category +=  (CategoryDic[Cat] + ' ,') ) 
    } catch (error) {
      this.Category = ''
    }
  }

  render(){
    const {Product} = this.props
    let ImgSource = `${CONSTANTS.BASE_API_URL}/thumb/${Product.thumbnail}`

    return (
      <div
        onClick={()=>this.setState({Detail : true})}
        className='col-md-4 col-sm-12 section__body'>
          <img
            src={ImgSource}
            alt = {Txt.AltProp}
            onError={()=>this.setState({PostImage : require('../../dist/images/postdefault.jpg')})}
            />
          <div className="content">
              <div className="title">{Product.title}</div>
          <div className="text">
            {this.Category}
          </div>
          <div className="pic-writer">
            { this.state.Detail 
              && 
              <ProductDetailCart 
                recordId={this.props.recordId} 
                ProductID = {Product.id} 
                close = {()=>this.setState({Detail : false})}
                fetchCart = {this.props.fetchCart}
                /> }
          </div>
          </div>
      </div>
    )
  }
}
//
export default ProductCardCart

//