import React,{  Component} from 'react';
//import { Txt, CONSTANTS } from '../../Utils'
import { Feed} from 'semantic-ui-react'
import {ProductDetail } from '..'
import './styles.css'


class ProductRender extends Component {
  constructor(props) {
    super(props)
    this.state = {
      Detail : false,
    }
  }

  render(){
  const {Item} = this.props
  console.log(Item);
  return (
      <div onClick={()=>this.setState({Detail : true})}>
        <Feed>
          <Feed.Event>
            <Feed.Content>
              <Feed.Summary>
                {Item.title}
              </Feed.Summary>
              <Feed.Extra text>
                {Item.price}
              </Feed.Extra>
            </Feed.Content>
          </Feed.Event>
        </Feed>
        { this.state.Detail && <ProductDetail ProductID = {Item.pid} close = {()=>this.setState({Detail : false})}/> }
      </div>
    )
  }
}
//
export default ProductRender
