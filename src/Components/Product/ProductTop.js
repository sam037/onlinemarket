/*
Author : MJS
 this Component will get and list all the product in main page
 props :
  *
*/
import React, { Component } from 'react';
import { connect } from 'react-redux'
import { ProductActions } from '../../Redux/Actions'
import { Txt } from '../../Utils'
import ProductCard from './ProductCard'
import { Dimmer, Loader, Image, Segment, Card} from 'semantic-ui-react'
import { Link } from 'react-router-dom'

class ProductTop extends Component {
  constructor(props){
    super(props);
    this.state = {
        Data : '',
        ProductLoading : true,
        List : '',
      }
    }
  componentDidMount(){
    this.props.dispatch(ProductActions.getTopProducts()).then(
      function(data){
            const {error,} = data;
            if(error){
                this.setState({error : true})
            } else {
                const Data = data.payload.product
                let List = []
                let KEY = 0;
                Data.forEach( (product) => {
                  List.push(
                    <ProductCard key= {KEY} Product = {product} />
                    //<p key= {KEY}><Link  to = {`/market/${product.id}`} > {product.name} </Link></p>
                  )
                  KEY = KEY + 1
                })
                this.setState({List , ProductLoading : false})


            }
          }.bind(this)
        )
  }
  render() {
    if (this.state.ProductLoading) return(
      <Segment>
        <Dimmer active>
          <Loader content= {Txt.Loading} />
        </Dimmer>
        <Image src= {require('../../Src/Image/paragraph.png')} />
      </Segment>
    )
    else return(
      <div className="container">
        <div className="main-title section__body">
            <div className="title"><b>محصولات برتر</b></div>
            <Link to='/blog' >
                <div className="more">{Txt.TopProduct}</div>
            </Link>
        </div>
        <div className="row">
          {this.state.List}
        </div>
      </div>
    )
  }
}

export default connect()(ProductTop);
//{this.state.isLoading && <Loading />}