import React, { Component } from 'react';
import { CONSTANTS,Txt } from '../../Utils'
import {connect } from 'react-redux'
import {ProductActions,ToastActions } from '../../Redux/Actions'
import { Modal, Button, Image, Segment} from 'semantic-ui-react'
import {Toast } from '..'

const inlineStyle = {
  modal : {
    marginLeft:   'auto',
    marginRight:  'auto',
    marginTop:    'auto !important',
    display:      'inline-block !important',
    position:     'relative',
    top:          '20%',
  }
};

class ProductDetailCart extends Component {
  constructor(props){
    super(props)
    this.state = {
      isLoading : true,
      Product : [],
      error : false,
    }
    this.ImageComponent = []
  }
  componentDidMount(){
    this.fetch()
  }
  fetch = ()=> {
    const { dispatch } = this.props
    dispatch(ProductActions.getProductDetail(this.props.ProductID)).then(
        function(data){
              const { error } = data;
              if(error){
                this.setState({error : true})
              } else {
                let Product = data.payload.Product
                this.setState({Product, isLoading : false})
                }
              }.bind(this)
        )
  }
  requestDownload = ()=>{
    const { dispatch } = this.props
    dispatch(ProductActions.requestDownload(this.props.ProductID))
  }
  render() {
    const {Product} = this.state
    if(this.state.isLoading){
      return(
        <Segment loading>
        </Segment>
      )
    } else {
      let ImgSource = `${CONSTANTS.BASE_API_URL}/thumb/${Product.thumbnail}`
      return(
        <Modal 
          open = {true} 
          onClose={this.props.close} 
          Loading = {this.state.isLoading}
          style={inlineStyle.modal}
          >
          <Modal.Header style = {{textAlign: 'center'}} >{Product.title}</Modal.Header>
          <Modal.Content image className ='rightToleft'>
            <Image
              wrapped
              src={ImgSource}
              alt = {Txt.AltProp}
              size='medium'
              />
            <Modal.Description>
              <p>{Product.description}</p>
            </Modal.Description>
          </Modal.Content>
          <Toast />
          <Button
            color='green'
            onClick = {this.requestDownload}
            fluid>{Txt.Download}
          </Button>
        </Modal>
      )
    }
  }
}

export default connect()(ProductDetailCart);
