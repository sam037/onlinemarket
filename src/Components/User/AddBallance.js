/*
Author : MJS

*/

import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Modal, Button, Icon } from 'semantic-ui-react';
import { UserActions } from '../../Redux/Actions'
import { Txt } from '../../Utils'

class AddBallance extends Component {
  constructor(props){
    super(props);
    this.state = {
      EnteredAmount : 0,
      FailedAddBallance : false,
      SuccededAddBallance : false,
      AddBallanceModal    : false,
    }
  }

  AmountOnChange = (event) => {
    this.setState({EnteredAmount : parseInt(event.target.value,10)})
  }
  AddBallanceFunction = () => {
    const { dispatch } = this.props
    dispatch(UserActions.addBallance(this.state.EnteredAmount)).then(
      function(data){
          const {error,} = data;
          if(error){
              this.setState({FailedAddBallance : true , SuccededAddBallance : false})
          } else {
              this.setState({FailedAddBallance : false, SuccededAddBallance : true})

            }
          }.bind(this)
    )

  }

  render() {
    return (
      <div id="addBallance">
        <Modal
          trigger = {
            <a className="ui left red corner label">
                <i className="icon plus circle"></i>
            </a>
          }
          onClose = {()=>this.setState({AddBallanceModal : false})}
          closeIcon>
          <Modal.Content>
            <p> How Much Money you want to add ?</p>
          </Modal.Content>
          <Modal.Actions>
            <input placeholder = 'Amount you want to add' onChange = {this.AmountOnChange}/>
            <Button onClick = {this.AddBallanceFunction}> ADD </Button>
            {this.state.SuccededAddBallance ? <Icon name = 'check circle' /> : <div></div>}
            {this.state.FailedAddBallance ? <Icon name = 'check circle' /> : <div></div>}
          </Modal.Actions>
        </Modal>
        <div className="ui row">
          <div className="ui basic label fluid rightToleft">
            <i className="globe icon"></i>
            {Txt.Ballance} :
            <a className="detail" id="ballance" > {this.props.Ballance}</a>
          </div>
        </div>
      </div>

    );
  }
}
const mapStateToProps = (state) => {
  return{
  AvailableBallance : state.User.profile.availableBallance,
  FullBalance       : state.User.profile.fullBallance
}}
export default connect(mapStateToProps)(AddBallance);

/*

*/
