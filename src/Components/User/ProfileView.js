/*
Author : MJS
*/
import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Txt } from '../../Utils'
import { Card, Header } from 'semantic-ui-react';
import { AddBallance } from '..'

class ProfileView extends Component {
  constructor(props){
    super(props);
    this.state = {
        Data : '',
        ProductLoading : true,
        User : '',
        update : true,
      }
  }
  componentDidUpdate(newProps){
    if(this.props.Profile.ballance !== newProps.Profile.ballance ){
      this.setState({update : !this.state.update})
    }
  }
  render() {
    if(this.props.UsrID === null) {
      return (
      <Card>
        <Card.Content header= {Txt.ProfileDetail} />
        <Card.Description>
          <Header as='h4' className = 'rightToleft'>
            {Txt.PleaseLogIn}
          </Header>
        </Card.Description>
        <Card.Content extra>
          <Header as='h5' className = 'rightToleft'>
          </Header>
        </Card.Content>
        <div className="ui segment red">
        </div>
      </Card>)
    }
    return(
      <Card>
        <Card.Content header= {Txt.ProfileDetail} />
        <Card.Description>
          <Header as='h4' className = 'rightToleft'>
            {Txt.Email} : {this.props.Profile.email}
          </Header>
        </Card.Description>
        <Card.Content extra>
          <Header as='h5' className = 'rightToleft'>
            {Txt.PaperCount}:{parseInt(this.props.Profile.count,10)}
          </Header>
        </Card.Content>
        <div className="ui segment red">
          <AddBallance Ballance = {this.props.Profile.ballance} />
        </div>
      </Card>
    )
  }
}
const mapStateToProps = (state) => {
  return {  UsrID   : state.Authentication.user_id,
            Profile : state.User.profile}
}
export default connect(mapStateToProps)(ProfileView)
