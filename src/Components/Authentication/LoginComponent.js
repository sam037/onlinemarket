import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { AuthActions, ToastActions} from '../../Redux/Actions'
import { Txt } from '../../Utils'
import { Loading } from '..'

class LoginComponent extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            isLoading           : false,

            LoginNotValid       : false,

            User                : '',
            UserInValid         : false,
            UserLengthInValid   : false,
            UserCharInValid     : false,
            UserStartInValid    : false,

            Pass                : '',
            PassInValid         : false,
            PassLengthInValid   : false,
            PassCharInValid     : false,
            PassStartInValid    : false,
        }
    }
    componentDidMount(){
    }
    UserOnChange = (event) => {
        let user = event.target.value.trim()
        var reLength = /^[ A-Za-z0-9_@.#&+-]{4,24}$/i;
        var reChar = /^[A-Za-z0-9_.]*$/i;
        var reStart = /^[a-zA-z][A-Za-z0-9_@.#&+-]*$/i;
        var reLengthValid = reLength.test(user)
        var reCharValid = reChar.test(user)
        var reStartValid = reStart.test(user)

        if(reLengthValid && reCharValid && reStartValid){
            this.setState({
                User                : user,
                UserLengthInValid   : !reLengthValid ,
                UserCharInValid     : !reCharValid ,
                UserStartInValid    : !reStartValid,
            })
        } else {
            this.setState({
                UserLengthInValid   : !reLengthValid ,
                UserCharInValid     : !reCharValid ,
                UserStartInValid    : !reStartValid,
            })
        }
    }
    PassOnChange = (event) => {
        let pass = event.target.value.trim()
        var reLength = /^[ A-Za-z0-9_@.#&+-]{4,24}$/i;
        var reChar = /^[A-Za-z0-9_.]*$/i;
        var reStart = /^[a-zA-z][A-Za-z0-9_@.#&+-]*$/i;
        var reLengthValid = reLength.test(pass)
        var reCharValid = reChar.test(pass)
        var reStartValid = reStart.test(pass)
        // eslint-disable-next-line
        let RepPassInValid = (this.state.RepPass == pass)
        if(reLengthValid && reCharValid && reStartValid){
            this.setState({
                Pass                : pass,
                RepPassInValid      : RepPassInValid,
                PassLengthInValid   : !reLengthValid,
                PassCharInValid     : !reCharValid,
                PassStartInValid    : !reStartValid,
                })
        } else {
            this.setState({
                RepPassInValid      : RepPassInValid,
                PassLengthInValid   : !reLengthValid,
                PassCharInValid     : !reCharValid,
                PassStartInValid    : !reStartValid,
                })
        }
    }
    AskForLogin = () => {
        const { dispatch } = this.props
        const { Pass ,User } = this.state
        this.setState({isLoading : true})
        if(Pass !== '' && User !== ''){
        dispatch(AuthActions.signIn(User,"username",Pass)).then(
            (data) => {
                const {error,} = data;
                if(error){
                    this.setState({
                        isLoading : false,
                        LoginNotValid :   true,
                    })
                } else {
                    this.setState({proceed : true})
                    this.props.close()
                }
                }
            )
        } else {
            dispatch(ToastActions.ShowToast(Txt.FillUserPass))
            this.setState({isLoading : false})
        }
    }
    _handleKeyPress = (e) => {
        if (e.key === 'Enter') {
          this.AskForLogin()
        }
    }
    render() {
        return (
        <div className="login">
            {this.state.isLoading && <Loading />}
            <div className="form-box">
                <center>
                <img src="dist/images/svg/logo.svg" alt="logo"/>
                <span className="company--name">شرکت بین المللی توسعه سرمایه <b>آرتام</b></span>
                </center>
                <h3>ورود</h3>
                <label className="label">
                <input 
                    type="text" 
                    onChange = {this.UserOnChange}
                    required="required" 
                    placeholder=" شناسه کاربری " 
                    className="input"/>
                    {this.state.UserLengthInValid && 
                        <span className="validate-error"> {Txt.UserLength}</span>}
                    {this.state.UserCharInValid && 
                        <span className="validate-error"> {Txt.UserChar}</span>}
                    {this.state.UserStartInValid && 
                        <span className="validate-error"> {Txt.UserStart}</span>}
                </label>
                <label className="label">
                    <input 
                        type="password" 
                        onChange = {this.PassOnChange}
                        required="required"
                        onKeyPress={this._handleKeyPress} 
                        placeholder="رمز عبور" 
                        className="input"/>
                    {this.state.PassLengthInValid && 
                        <span className="validate-error"> {Txt.PassLength}</span>}
                    {this.state.PassCharInValid && 
                        <span className="validate-error"> {Txt.PassChar}</span>}
                    {this.state.PassStartInValid && 
                        <span className="validate-error"> {Txt.PassStart}</span>}
                </label>
                <div className="control">
                    <input 
                        id="reminder" 
                        type="checkbox" 
                        name="reminder" 
                        className="control__input blue"/>
                </div>
                <label className="label">
                {this.state.LoginNotValid && <span className="validate-error"> {Txt.UserNotValid}</span>}
                <button className="green-btn btn" onClick ={this.AskForLogin}>
                    ورود
                </button>
                </label>
                <label className="label">
                <div className="notice">
                    آیا رمز عبور خودرا فراموش کردید؟
                    <a onClick = {()=> this.props.ChangeScreen('ResetPassword')} >بازیابی رمز عبور</a>
                </div>
                </label>
                <label className="label">
                <div className="notice">
                    قبلا در آرتام ثبت‌نام نکرده‌اید؟
                    <Link to='/signup'> ثبت‌نام در آرتام </Link>
                </div>
                </label>
            </div>
            </div>
        );
    }
}

const mapStatetoProps = (state) => {
  return({
  })
}
export default connect(mapStatetoProps)(LoginComponent)
//<label htmlFor="reminder" className="control__label">مرا به خاطر بسپار</label>