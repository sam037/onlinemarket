// Author = MJ SAM
import React, { Component } from 'react'
import {Timer} from '..'
import { connect } from 'react-redux'
import { AuthActions, ToastActions} from '../../Redux/Actions'
import { Redirect } from 'react-router-dom'
import Txt from '../../Utils/txtConstant'

class VerifyComponent extends Component {
  constructor(props){
    super(props)
    this.state = {
      isLoading : false,
      proceed   : false,
      timesUp   : false,
      Code      : '',
      Button    : false,
      CodeValid : true,
      redirect : false,

    }
  }

  CodeOnChange = (event) => {
    let verifycode = event.target.value.trim()
    var re = /^[0-9]{5}$/;
    var verifycodevalid = re.test(verifycode)
    if(verifycodevalid){
      this.setState({
        Code      : verifycode,
        CodeValid : verifycodevalid,
        },()=>{
          if(this.state.Code.length === 5){
            this.AskToVerifyPhone()
          }
        })
    } else {
      this.setState({ CodeValid : verifycodevalid, })
    }
  }
  
  AskToVerifyPhone = () => {
    const { dispatch  } = this.props
    // eslint-disable-next-line
    if(this.state.CodeValid && this.state.Code != ''){
      dispatch(AuthActions.verifyCode(this.props.UserInfo,this.state.Code)).then( 
        (data) => {
            const {error,payload} = data;
            if(error) {
              this.setState({isLoading : false,
                              code : ''})
              dispatch(ToastActions.ShowToast(' کد اشتباه است '))
            } else {
              this.setState({redirect : true})
            }
          }
    )}
  }

  AskForResendCode = () => {
    const { dispatch } = this.props
    this.setState({Button : false})
    dispatch(AuthActions.resendCode(this.props.UserInfo))
  }
  
  render() {
    if(this.state.redirect){return(<Redirect to='/' />)}
    return (
      <div className="container">
          <div className="form-box">
            <h3>اهراز هویت</h3>
            <div>
              <p>به شماره تلفن <b>{this.props.Phone}</b> کدی ارسال شده است، کد ارسالی را وارد کنید.</p>
              <div className="row">
                <div className="col-sm-12 col-lg-12">
                  <label htmlFor="otp" className="label">کد ارسالی<span title="این فیلد اجباری است." className="form-required">*</span></label>
                  <input 
                    type="text" 
                    onChange = {this.CodeOnChange}
                    required="required" 
                    id="otp" 
                    name="otp" 
                    placeholder="کد ارسالی را وارد کنید..." 
                    className="required input mb-1 erorr"/>
                </div>
              </div>
              <label className="label">
                <div className="notice">کدی بدستم نرسیده است.
                  {this.state.Button ? (
                    <a onClick = {this.AskForResendCode}>
                      {Txt.ReSend}
                    </a>
                  ):(
                    <Timer seconds = {30}
                        Text = {Txt.TryAgain}
                        AfterTimer = {() => this.setState({Button : true}) } />
                  )}
                </div>
              </label>
              <label className="label mb-0">
                <button className="green-btn btn" onClick ={this.AskToVerifyPhone}>
                  {Txt.Verify}
                </button>
              </label>
            </div>
          </div>
        </div>
    )
  }
}

export default connect()(VerifyComponent)
