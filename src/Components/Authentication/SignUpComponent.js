/*
Author : MJS
props :
*/
import React, { Component } from 'react';
//import { UserProfileHeader, Header , Founder } from '..'
import { connect } from 'react-redux'
//import { Redirect } from 'react-router-dom'
import { AuthActions , ToastActions } from '../../Redux/Actions'
import { Txt} from '../../Utils'
import { Redirect } from 'react-router';
import { Icon } from 'semantic-ui-react'

class SignUp extends Component {
    constructor(props){
    super(props);

    this.state = {
        refferalInvalid : false ,

        ChekingUser :   'notCheking',
        ChekingPhone:   'notCheking',

        Phone       : '',
        PhoneValidF : false,
        PhoneValid  : true,
        
        User                : '',
        UserValid         : false,
        UserLengthValid   : true,
        UserCharValid     : true,
        UserStartValid    : true,


        Pass        : '',
        PassValid : false,
        PassLengthValid   : true,
        PassCharValid     : true,
        PassStartValid    : true,

        
        RepPass         : '',
        RepPassValid  : true,
    }
    }
    componentDidMount(){
    }
    FirstnameOnChange = (event) => {
        let name = event.target.value.trim()
        var CheckName = /^[پچجحخهعغآ؟.،آفقثصضشسیبلاتنمکگوئدذرزطظژ!!ؤإأءًٌٍَُِّ a-zA-Z\s]{4,24}$/u    
        var nameChecked = CheckName.test(name)
        if(nameChecked){
            this.setState({
                FirstName : name,
                FirstNameValid : false,
            })
        } else {
            this.setState({
                FirstNameValid : true
            })
        }
    }
    LastNameValid = (event) => {
        let name = event.target.value.trim()
        var CheckName = /^[پچجحخهعغآ؟.،آفقثصضشسیبلاتنمکگوئدذرزطظژ!!ؤإأءًٌٍَُِّ a-zA-Z\s]{4,24}$/u    
        var nameChecked = CheckName.test(name)
        if(nameChecked){
            this.setState({
                LastName : name,
                LastNameValid : false,
            })
        } else {
            this.setState({
                LastNameValid : true
            })
        }
    }
    PhoneOnChange = (event) => {
        let phone = event.target.value.trim()
        var re = /^[0-9]{11}$/;
        var phonevalid = re.test(phone)
        if(phonevalid){
            this.setState({
                Phone : phone ,
                PhoneValidF: true,
                PhoneValid : phonevalid
            })
        // eslint-disable-next-line
        } else if(phone == ''){
                this.setState({
                    PhoneValid : true
                })
        } else {
            this.setState({
                PhoneValid : phonevalid
            })
        }
    }
    UserOnChange = (event) => {
        let user = event.target.value.trim()
        var reLength = /^[ A-Za-z0-9_@.#&+-]{4,24}$/i;
        var reChar = /^[A-Za-z0-9_.]*$/i;
        var reStart = /^[a-zA-z][A-Za-z0-9_@.#&+-]*$/i;
        var reLengthValid = reLength.test(user)
        var reCharValid = reChar.test(user)
        var reStartValid = reStart.test(user)
        if(reLengthValid && reCharValid && reStartValid){
            this.setState({
                User                : user,
                UserValid         : true,
                UserLengthValid   : reLengthValid ,
                UserCharValid     : reCharValid ,
                UserStartValid    : reStartValid,
            })
            // eslint-disable-next-line
        } else if(user == ''){
            this.setState({
                UserLengthValid   : true ,
                UserCharValid     : true ,
                UserStartValid    : true,
            })
        } else {
            this.setState({
                UserLengthValid   : reLengthValid ,
                UserCharValid     : reCharValid ,
                UserStartValid    : reStartValid,
            })
        }
    }
    PassOnChange = (event) => {
        let pass = event.target.value.trim()
        var reLength = /^[ A-Za-z0-9_@.#&+-]{4,24}$/i;
        var reChar = /^[A-Za-z0-9_.]*$/i;
        var reStart = /^[a-zA-z][A-Za-z0-9_@.#&+-]*$/i;
        var reLengthValid = reLength.test(pass)
        var reCharValid = reChar.test(pass)
        var reStartValid = reStart.test(pass)
        // eslint-disable-next-line
        let RepPassValid = (this.state.RepPass == pass)
        if(reLengthValid && reCharValid && reStartValid){
            this.setState({
                Pass                : pass,
                PassValid           : true,
                RepPassValid      : RepPassValid,
                PassLengthValid   : reLengthValid,
                PassCharValid     : reCharValid,
                PassStartValid    : reStartValid,
                })
            // eslint-disable-next-line
        } else if(pass == ''){
            this.setState({
                PassLengthValid   : true,
                PassCharValid     : true,
                PassStartValid    : true,
                })
        } else {
            this.setState({
                RepPassValid      : RepPassValid,
                PassLengthValid   : reLengthValid,
                PassCharValid     : reCharValid,
                PassStartValid    : reStartValid,
                })
        }
    }
    RepPassOnChange = (event) => {
        let repeatPass = event.target.value
        // eslint-disable-next-line
        if(repeatPass == this.state.Pass && event.target.value != '') {
            this.setState({
                RepPass : repeatPass,
                RepPassValid : true 
            })
        }
        else {
            this.setState({
                RepPass : repeatPass,
                RepPassValid : false
            })
        }
    }
    CheckForUser = (event) => {
        const { dispatch } = this.props
        // eslint-disable-next-line
        if(this.state.UserValid && this.state.User != ''){
            this.setState({ChekingUser : 'Checking'})
            dispatch(AuthActions.CheckUser(event.target.value.trim())).then(
                (res) => {
                    if(res.error){
                        this.setState({ChekingUser : 'CheckedNotValid'})
                    } else {
                        this.setState({ChekingUser : 'CheckedValid'})
                    }
                }
            )
        }
    }
    CheckForPhone = (event) => {
        const { dispatch } = this.props
        // eslint-disable-next-line
        if(this.state.PhoneValid && this.state.Phone != ''){
            this.setState({ChekingPhone : 'Checking'})
            dispatch(AuthActions.CheckPhone(event.target.value.trim())).then(
                (res) => {
                    if(res.error){
                        this.setState({ChekingPhone : 'CheckedNotValid'})
                    } else {
                        this.setState({ChekingPhone : 'CheckedValid'})
                    }
                }
            )
        }
    }
    AskForSignUp = () => {
        const { dispatch } = this.props
        const { Pass ,Phone,User,PhoneValidF,PassValid,RepPassValid,UserValid} = this.state
        if(PhoneValidF && PassValid && RepPassValid && UserValid ){
            this.setState({isLoading : true})
            dispatch(AuthActions.signUp(User, Phone, Pass)).then((data) => {
                    const {error,payload , redirect} = data;
                    if(redirect) {
                        this.setState({isLoading : false ,});
                        this.props.push(`/signup`)
                    } else if(error){
                        this.setState({isLoading : false });
                    } else {
                        this.props.updateUserInfo(payload.user_info,()=>this.props.ChangeScreen('Verify'),Phone)
                }
            })
        } else {
          dispatch(ToastActions.ShowToast(Txt.All))
          this.setState({UserNameErr : true , PasswordErr : true , RepPassErr : true , PhoneErr : true })
        }
    }
    _handleKeyPress = (e) => {
        if (e.key === 'Enter') {
          this.AskForSignUp()
        }
    }
    render() {
        let UserChecked = null
        let PhoneChecked = null
        // eslint-disable-next-line
        if(this.state.ChekingUser == 'notChecked'){
            UserChecked = <div className="icon__place"></div>
            // eslint-disable-next-line
        } else if(this.state.ChekingUser == 'Checking'){
            UserChecked = <div className="icon__place"><Icon loading name='spinner' /></div>
            // eslint-disable-next-line
        } else if(this.state.ChekingUser == 'CheckedValid'){
            // eslint-disable-next-line
            UserChecked = <div className="icon__place"><Icon  color='green' name='check circle' /></div>
            // eslint-disable-next-line
        } else if(this.state.ChekingUser == 'CheckedNotValid'){
            // eslint-disable-next-line
            UserChecked = <div className="icon__place"><Icon  color='red' name='times circle' /></div>
        }
        // eslint-disable-next-line
        if(this.state.ChekingPhone == 'notChecked'){
            PhoneChecked = <div className="icon__place"></div>
            // eslint-disable-next-line
        } else if(this.state.ChekingPhone == 'Checking'){
            PhoneChecked = <div className="icon__place"><Icon loading name='spinner' /></div>
            // eslint-disable-next-line
        } else if(this.state.ChekingPhone == 'CheckedValid'){
            PhoneChecked = <div className="icon__place"><Icon  color='green' name='check circle' /></div>
            // eslint-disable-next-line
        } else if(this.state.ChekingPhone == 'CheckedNotValid'){
            PhoneChecked = <div className="icon__place"><Icon  color='red' name='times circle' /></div>
        }
        if(this.props.UsrID) return <Redirect to='/'/>
        if(this.state.refferalInvalid) return <Redirect to='/signup'/>
        return (    
            <div className="container">
                <div className="form-box">
                <h3>ثبت نام</h3>
                    <div className="row">
                    <div className="col-sm-12 col-lg-12">
                        <label htmlFor="username" className="label">
                            نام کاربری
                        <span title="این فیلد اجباری است." className="form-required">*</span></label>
                        <input 
                            type="text"
                            onChange = {this.UserOnChange}
                            required="required" 
                            id="username" 
                            name="username" 
                            onBlur = {this.CheckForUser}
                            placeholder="نام کاربری را وارد کنید..." 
                            className="required input mb-1 erorr"/>
                            { UserChecked }
                            {!this.state.UserLengthValid &&
                                <span className="validate-error"> {Txt.UserLength}</span>}
                            {!this.state.UserCharValid && 
                                <span className="validate-error"> {Txt.UserChar}</span>}
                            {!this.state.UserStartValid && 
                                <span className="validate-error"> {Txt.UserStart}</span>}
                    </div>
                    <div className="col-sm-12 col-lg-12">
                        <label 
                            htmlFor="phone" 
                            className="label">شماره موبایل
                        <span title="این فیلد اجباری است." className="form-required">*</span></label>
                        <input 
                            type="phone"
                            onChange = {this.PhoneOnChange}
                            id="phone" 
                            required="required" 
                            name="phone" 
                            onBlur = {this.CheckForPhone}
                            placeholder="شماره موبایل را وارد کنید..." 
                            className="required input mb-1 erorr"/>
                            {PhoneChecked}
                            {!this.state.PhoneValid &&
                                <span className="validate-error"> {Txt.EnterValidPhone}</span>}
                    </div>
                    <div className="col-sm-12 col-lg-6">
                        <label htmlFor="password" className="label">کلمه عبور<span title="این فیلد اجباری است." className="form-required">*</span></label>
                        <input 
                            type="password" 
                            onChange = {this.PassOnChange}
                            required="required" 
                            id="password" 
                            name="password" 
                            placeholder="کلمه عبور را وارد کنید..." 
                            className="required input mb-1 erorr"/>
                            
                            {!this.state.PassLengthValid &&
                                <span className="validate-error"> {Txt.PassLength}</span>}
                            {!this.state.PassCharValid && 
                                <span className="validate-error"> {Txt.PassChar}</span>}
                            {!this.state.PassStartValid && 
                                <span className="validate-error"> {Txt.PassStart}</span>}
                    </div>
                    <div className="col-sm-12 col-lg-6">
                        <label 
                            htmlFor="confirm_password" 
                            className="label">تکرار عبور
                            <span 
                                title="این فیلد اجباری است." 
                                className="form-required">*
                            </span>
                        </label>
                        <input 
                            type="password" 
                            onChange = {this.RepPassOnChange}
                            required="required" 
                            id="confirm_password" 
                            name="confirm_password" 
                            placeholder="تکرار عبور را وارد کنید..."
                            onKeyPress={this._handleKeyPress} 
                            className="required input mb-1 erorr"/>
                        {!this.state.RepPassValid && 
                                <span className="validate-error"> {Txt.RepPMustBeSame}</span>}
                    </div>
                    </div>
                    <label className="label">
                        <button className="green-btn btn" onClick={this.AskForSignUp}>ثبت نام</button>
                    </label>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {  UsrID: state.Authentication.user_id,};
  };
  
  export default connect(mapStateToProps)(SignUp)