// Author = MJ SAM

import React, { Component } from 'react'
import { connect } from 'react-redux'
import { AuthActions,ToastActions} from '../../Redux/Actions'
import Txt from '../../Utils/txtConstant'
import { Dropdown } from 'semantic-ui-react'
import { Loading } from '..'

class ResetPassword extends Component {
  constructor(props){
      super(props)
      this.state = {
          isLoading : false,
          proceed : false,
          Phone       : '',
          PhoneValidF : false,
          PhoneValid  : true,
          
          User                : '',
          UserValid         : false,
          UserLengthValid   : true,
          UserCharValid     : true,
          UserStartValid    : true,
          
          Email             : '',
          EmailValid        : true,

          type : 'username',
          input : '',
        }
      this.Vars = {
        validInput : false
      }
      this.countryOptions  = [
          { text: '  نام کاربری  ', value: 'username',
            image: { avatar: true, src: require('../../dist/images/icons/user.png') },
          },
          { text: '  تلفن همراه  ', value: 'phone',
            image: { avatar: true, src: require('../../dist/images/icons/mobile.png') },
          },
          { text: '  پست الکترونیک  ', value: 'email',
            image: { avatar: true, src: require('../../dist/images/icons/email.png') },
          },

         ]
    }

  AskForResetPassword = () => {
    const { dispatch } = this.props
    dispatch(AuthActions.resetPassword(this.state.type,this.state.input)).then(
    function(data){
          const {error,payload} = data;
          if(error) {
            dispatch(ToastActions.ShowToast(payload.message))
          } else {
            this.props.ChangeScreen('Login')
          }
        }.bind(this) )
  }

  setInput = (event) => {
    this.Vars.input = event.target.value
  }

  componentDidMount() {
  }

  componentDidUpdate(){
  }

  componentWillReceiveProps(nextprops){
  }

  selectType = (e,data) =>{
    this.setState({type : data.value})
  }

  PhoneOnChange = (event) => {
    let phone = event.target.value.trim()
    var re = /^[0-9]{11}$/;
    var phonevalid = re.test(phone)
    if(phonevalid){
        this.setState({
            Phone : phone ,
            input : phone,
            PhoneValidF: true,
            PhoneValid : phonevalid
        })
    // eslint-disable-next-line
    } else if(phone == ''){
            this.setState({
                PhoneValid : true
            })
    } else {
        this.setState({
            PhoneValid : phonevalid
        })
    }
  }

  UserOnChange = (event) => {
      let user = event.target.value.trim()
      var reLength = /^[ A-Za-z0-9_@.#&+-]{4,24}$/i;
      var reChar = /^[A-Za-z0-9_.]*$/i;
      var reStart = /^[a-zA-z][A-Za-z0-9_@.#&+-]*$/i;
      var reLengthValid = reLength.test(user)
      var reCharValid = reChar.test(user)
      var reStartValid = reStart.test(user)
      if(reLengthValid && reCharValid && reStartValid){
          this.setState({
              User                : user,
              input             : user,
              UserValid         : true,
              UserLengthValid   : reLengthValid ,
              UserCharValid     : reCharValid ,
              UserStartValid    : reStartValid,
          })
      // eslint-disable-next-line
      } else if(user == ''){
          this.setState({
              UserLengthValid   : true ,
              UserCharValid     : true ,
              UserStartValid    : true,
          })
      } else {
          this.setState({
              UserLengthValid   : reLengthValid ,
              UserCharValid     : reCharValid ,
              UserStartValid    : reStartValid,
          })
      }
  }
  EmailOnChange = (event) => {
      let email = event.target.value.trim()
      var reEmail = /^[a-z][a-zA-Z0-9_.]*(\.[a-zA-Z][a-zA-Z0-9_.]*)?@[a-z][a-zA-Z-0-9]*\.[a-z]+(\.[a-z]+)?$/i;
      let validEmail = reEmail.test(email)
      if(validEmail){
          this.setState({
              input : email,
              Email : email,
              EmailInValid : false,
          })
      // eslint-disable-next-line
      } else if(email == '') {
          this.setState({
              EmailInValid : true
          })
      } else {
        this.setState({
            EmailInValid : true
        })
    }
  }
  render() {
    let resetTypeInput = null
    switch (this.state.type) {
      case 'username':
        resetTypeInput = (
            <label className="label">
              <input 
                    type="text"
                    onChange = {this.UserOnChange}
                    required="required" 
                    id="username" 
                    name="username" 
                    onBlur = {this.CheckForUser}
                    placeholder="نام کاربری را وارد کنید..." 
                    className="required input mb-1 erorr"/>
                    {!this.state.UserLengthValid &&
                        <span className="validate-error"> {Txt.UserLength}</span>}
                    {!this.state.UserCharValid && 
                        <span className="validate-error"> {Txt.UserChar}</span>}
                    {!this.state.UserStartValid && 
                        <span className="validate-error"> {Txt.UserStart}</span>}
            </label>
          )
        break;
      case 'phone':
        resetTypeInput = (
          <label htmlFor="phone"  className="label">
            <input 
              type="phone"
              onChange = {this.PhoneOnChange}
              id="phone" 
              required="required" 
              name="phone" 
              onBlur = {this.CheckForPhone}
              placeholder="شماره موبایل را وارد کنید..." 
              className="required input mb-1 erorr"/>
              {!this.state.PhoneValid &&
                  <span className="validate-error"> {Txt.EnterValidPhone}</span>}
          </label>)
        break
      case 'email':
        resetTypeInput = (
          <label htmlFor="email"  className="label">
            <input 
              type="text" 
              onChange = {this.EmailOnChange}
              id="email" 
              name="mail"
              placeholder="آدرس ایمیل را وارد کنید..." 
              className="input mb-1 erorr"/>
              {!this.state.EmailValid &&
                    <span className="validate-error"> {Txt.EnterValidEmail}</span>}
          </label>)
        break
      default:
        break;
    }
    return (
      <div className="login">
        {this.state.isLoading && <Loading />}
        <div className="form-box">
          <center>
            <img src="dist/images/svg/logo.svg" alt="logo"/>
            <span className="company--name">شرکت بین المللی توسعه سرمایه <b>آرتام</b></span>
          </center>
            <h3> فراموشی کلمه عبور</h3>
            <center>
              <Dropdown 
                direction = 'left'
                placeholder='Select Country' 
                selection 
                value = {this.state.type}
                onChange = {this.selectType}
                options={this.countryOptions} />
            </center>
            <br/>
              {
                resetTypeInput
              }
            <div className="notice">
                <a onClick = {()=> this.props.ChangeScreen('Login')} > بازگشت به صفحه ورود </a>
            </div>
            <center>
              <label className="label">
                <button className="green-btn btn" onClick ={this.AskForResetPassword}>
                    ارسال لینک
                </button>
              </label>
            </center>
        </div>
      </div>
    )
  }
}

export default connect()(ResetPassword)
