import React  from 'react'

export default class Timer extends React.Component {
  constructor (props) {
    super(props)
    this.state = {count: this.props.seconds}
  }
  componentWillMount () {
    clearInterval(this.timer)
    this.timer = setInterval(this.tick , 1000)
  }
  tick = () => {
    if(this.state.count >= 1) {
      const Next = this.state.count - 1
      this.setState({count: Next})

    } else {
      clearInterval(this.timer)
      this.props.AfterTimer()
    }
  }

  render () {
    return (
      <div className="ui small statistic">
        <div className="label">
          {this.props.text}
        </div>
        <div className="value">
          <i className="time icon"></i> {this.state.count}
        </div>
      </div>
    )
  }
}
