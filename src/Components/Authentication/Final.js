// Authors =  {logic : MJ SAM }
import React, { Component } from 'react'
import Txt from '../../Utils/txtConstant'
import {AuthActions,ToastActions} from '../../Redux/Actions'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { Tab } from 'semantic-ui-react'
import { Loading } from '..'
import { Icon } from 'semantic-ui-react'


class Final extends Component {
  constructor(props){
    super(props)
    this.state = {
      ChekingEmail      : 'notCheking',
      ChekingLNN        : 'notCheking',
      ChekingRNN        : 'notCheking',

      isLoading         : false ,

      FirstName         : '',
      FirstNameValid    : true,

      LastName          : '',
      LastNameValid     : true,

      Email             : '',
      EmailValid        : true,

      NNumber           : '',
      NNumberValid      : true,

      JobName           : '',
      JobNameValid      : true,

      FirmName          : '',
      FirmNameValid     : true,

      BirthDate         : '',
      BirthDateValid    : true,

      NSubmit           : '',
      NSubmitValid      : true,

      NNumberLegal      : '',
      NNumberLegalValid : true,

      Profile           : '',

      Procced           : false,

      CEOName           : '',
      CEONameValid      : true,

      FieldName         : '',
      FieldNameValid    : true,
      }
  }
  resetTab = () => {
    this.setState({
      isLoading         : false ,

      FirstName         : '',
      FirstNameValid    : true,

      LastName          : '',
      LastNameValid     : true,

      Email             : '',
      EmailValid        : true,

      NNumber           : '',
      NNumberValid      : true,

      JobName           : '',
      JobNameValid      : true,

      FirmName          : '',
      FirmNameValid     : true,

      BirthDate         : '',
      BirthDateValid    : true,

      NSubmit           : '',
      NSubmitValid      : true,

      NNumberLegal      : '',
      NNumberLegalValid : true,

      Profile           : '',

      Procced           : false,

      CEOName           : '',
      CEONameValid      : true,

      FieldName         : '',
      FieldNameValid    : true,
    })
  }
  CheckForEmail = (event) => {
    const { dispatch } = this.props
    // eslint-disable-next-line
    if(this.state.EmailValid && this.state.Email != ''){
        this.setState({ChekingEmail : 'Checking'})
        dispatch(AuthActions.CheckEmail(event.target.value.trim())).then(
            (res) => {
                if(res.error){
                    this.setState({ChekingEmail : 'CheckedNotValid'})
                } else {
                    this.setState({ChekingEmail : 'CheckedValid'})
                }
            }
        )
    }
  }
  // Real Person Function
  CheckForRNN = (event) => {
    const { dispatch } = this.props
    // eslint-disable-next-line
    if(this.state.NNumberValid && this.state.NNumber != ''){
        this.setState({ChekingRNN : 'Checking'})
        dispatch(AuthActions.CheckRNN(event.target.value)).then(
            (res) => {
                if(res.error){
                    this.setState({ChekingRNN : 'CheckedNotValid'})
                } else {
                    this.setState({ChekingRNN : 'CheckedValid'})
                }
            }
        )
    }
  }
  FirstnameOnChange = (event) => {
    let name = event.target.value.trim()
    var CheckName = /^[پچجحخهعغآ؟.،آفقثصضشسیبلاتنمکگوئدذرزطظژ!!ؤإأءًٌٍَُِّ \s]{2,24}$/u    
    var nameChecked = CheckName.test(name)
    if(nameChecked){
        this.setState({
          FirstName : name,
          FirstNameValid : true,
        })
        // eslint-disable-next-line
    } else if(name == ''){
        this.setState({
          FirstNameValid : true
        })
    } else {
      this.setState({
        FirstNameValid : false
      })
    }
  }
  LastnameOnChange = (event) => {
    let name = event.target.value.trim()
    var CheckName = /^[پچجحخهعغآ؟.،آفقثصضشسیبلاتنمکگوئدذرزطظژ!!ؤإأءًٌٍَُِّ \s]{2,24}$/u    
    var nameChecked = CheckName.test(name)
    if(nameChecked){
      this.setState({
        LastName : name,
        LastNameValid : true,
      })
      // eslint-disable-next-line
    } else if(name == ''){
        this.setState({
          LastNameValid : true
        })
    } else {
      this.setState({
        LastNameValid : false
      })
    }
  }
  EmailOnChange = (event) => {
    let email = event.target.value.trim()
    var reEmail = /^[a-z][a-zA-Z0-9_.]*(\.[a-zA-Z][a-zA-Z0-9_.]*)?@[a-z][a-zA-Z]*\.[a-z]+(\.[a-z]+)?$/i;
    let validEmail = reEmail.test(email)
    if(validEmail){
        this.setState({
            Email : email,
            EmailValid : true,
        })
        // eslint-disable-next-line
    } else if(email == '') {
        this.setState({
            EmailValid : true
        })
    } else {
      this.setState({
          EmailValid : false
      })
  }
  }
  
  NNumberOnChange = (event) => {
    let number = event.target.value
    var re = /^[0-9]{10}$/;
    var verifynumber = re.test(number)
    if(verifynumber){
      this.setState({
        NNumber      : number,
        NNumberValid : verifynumber,
        })
        // eslint-disable-next-line
    } else if(number == ''){
      this.setState({ NNumberValid : true, })
    } else {
      this.setState({ NNumberValid : verifynumber, })
    }
  }

  JobnameOnChange = (event) => {
    let name = event.target.value.trim()
    var CheckName = /^[پچجحخهعغآ؟.،آفقثصضشسیبلاتنمکگوئدذرزطظژ!!ؤإأءًٌٍَُِّ a-zA-Z\s]{1,50}$/u 
    var nameChecked = CheckName.test(name)
    if(nameChecked){
      this.setState({
        JobName         : name,
        JobNameValid    : nameChecked,
      })
      // eslint-disable-next-line
    } else if(name == ''){
      this.setState({
        JobNameValid : true
      })
    } else {
      this.setState({
        JobNameValid : false
      })
    }
  }

  FirmnameOnChange = (event) => {
    let name = event.target.value.trim()
    var CheckName = /^[پچجحخهعغآ؟.،آفقثصضشسیبلاتنمکگوئدذرزطظژ!!ؤإأءًٌٍَُِّ a-zA-Z\s]{1,50}$/u 
    var nameChecked = CheckName.test(name)
    if(nameChecked){
      this.setState({
        FirmName        : name,
        FirmNameValid   : true,
      })
      // eslint-disable-next-line
    } else if(name == ''){
        this.setState({
          FirmNameValid : true
        })
    } else {
      this.setState({
        FirmNameValid : false
      })
    }
  }
  BirthdateOnChange = (event) => {
    let date = event.target.value
    var re = /^([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))$/;
    var dateChecked = re.test(date)
    if(dateChecked){
      this.setState({
        BirthDate        : date,
        BirthDateValid   : true,
      })
      // eslint-disable-next-line
    } else if(date == ''){
        this.setState({
          BirthDateValid : true
        })
    } else {
      this.setState({
        BirthDateValid : false
      })
    }
  }
  //=============== Legal Person Function ======================
  CheckForLNN = (event) => {
    const { dispatch } = this.props
    // eslint-disable-next-line
    if(this.state.NNumberLegalValid && this.state.NNumberLegal != ''){
        this.setState({ChekingLNN : 'Checking'})
        dispatch(AuthActions.CheckLNN(event.target.value)).then(
            (res) => {
                if(res.error){
                    this.setState({ChekingLNN : 'CheckedNotValid'})
                } else {
                    this.setState({ChekingLNN : 'CheckedValid'})
                }
            }
        )
    }
  }
  NSubmitOnChange = (event) => {
    let number = event.target.value
    var re = /^[0-9]{0,20}$/;
    var verifynumber = re.test(number)
    if(verifynumber){
      this.setState({
        NSubmit      : number,
        NSubmitValid : verifynumber,
        })
        // eslint-disable-next-line
    } else if(number == ''){
      this.setState({ NSubmitValid : true, })
    } else {
      this.setState({ NSubmitValid : verifynumber, })
    }
  }
  
  NNumberLegalOnChange = (event) => {
    let number = event.target.value
    var re = /^[0-9]{0,20}$/;
    var verifynumber = re.test(number)
    if(verifynumber){
      this.setState({
        NNumberLegal      : number,
        NNumberLegalValid : verifynumber,
        })
        // eslint-disable-next-line
    } else if(number == ''){
      this.setState({ NNumberLegalValid : true, })
    } else {
      this.setState({ NNumberLegalValid : verifynumber, })
    }
  }
  CEOOnChange = (event) => {
    let name = event.target.value.trim()
    var CheckName = /^[پچجحخهعغآ؟.،آفقثصضشسیبلاتنمکگوئدذرزطظژ!!ؤإأءًٌٍَُِّ \s]{2,24}$/u    
    var nameChecked = CheckName.test(name)
    if(nameChecked){
        this.setState({
          CEOName : name,
          CEONameValid : true,
        })
        // eslint-disable-next-line
    } else if(name == ''){
        this.setState({
          CEONameValid : true
        })
    } else {
        this.setState({
          CEONameValid : false
        })
    }
  }

  FieldOnChange = (event) => {
    let name = event.target.value.trim()
    var CheckName = /^[پچجحخهعغآ؟.،آفقثصضشسیبلاتنمکگوئدذرزطظژ!!ؤإأءًٌٍَُِّ \s]{0,50}$/u    
    var nameChecked = CheckName.test(name)
    if(nameChecked){
        this.setState({
          FieldName : name,
          FieldNameValid : true,
        })
        // eslint-disable-next-line
    } else if(name == ''){
        this.setState({
          FieldNameValid : true
        })
    } else {
        this.setState({
          FieldNameValid : false
        })
    }
  }
  //var rePersian = /^[\u0600-\u06FF]+\s{0,25}$/i;
  //======= finalizing the registeration =======

  sendFinalReal = () => {
    const {dispatch} = this.props
    // eslint-disable-next-line
    if((this.state.FirstNameValid || this.state.FirstName !='') &&
      // eslint-disable-next-line
      (this.state.LastNameValid || this.state.LastName !='') &&
      // eslint-disable-next-line
      (this.state.NNumberValid || this.state.NNumber !='') ){
      this.setState({isLoading : true})
      let Data = {
      'firstName' : this.state.FirstName,
      'lastName'  : this.state.LastName,
      'email'     : this.state.Email,
      'birthDate' : this.state.BirthDate,
      'nNational' : this.state.NNumber,
      'work'      : this.state.JobName,
      'workPlace' : this.state.FirmName,
      'profile'   : this.state.Profile,
      }
      dispatch(AuthActions.finalReal(this.props.UserInfo,Data)).then((res)=>{
        console.log(res);
        
        if(!res.error) {
          this.setState({Procced : true})
        } else {
          dispatch(ToastActions.ShowToast(Txt.ProblemWithConnection))
          this.setState({isLoading : false})
        }
      })
    } else {
        dispatch(ToastActions.ShowToast(' تمامی موارد الزامی باید تکمیل گردد '))
    }
  }
  sendFinalLegal = () => {
    const {dispatch} = this.props
    // eslint-disable-next-line
    if((this.state.FirmNameValid || this.state.FirmName !='' )&&
    // eslint-disable-next-line
      (this.state.NSubmitValid || this.state.FirmName !='') &&
      // eslint-disable-next-line
      (this.state.EmailValid || this.state.Email != '') &&
      // eslint-disable-next-line
      (this.state.NNumberLegalValid || this.state.NNumberLegal !='')&&
      // eslint-disable-next-line
      (this.state.BirthDateValid || this.state.BirthDate != '') ) {
        this.setState({isLoading : true})
        let Data = {
        'name'          : this.state.FirmName,
        'nSubmit'       : this.state.NSubmit,
        'email'         : this.state.Email,
        'submitDate'    : this.state.BirthDate,
        'nNational'     : this.state.NNumberLegal,
        'CEO'           : this.state.CEOName,
        'activityArea'  : this.state.FieldName,
        'profile'       : this.state.Profile,
      }
      dispatch(AuthActions.finalLegal(this.props.UserInfo,Data)).then((res)=>{
        if(!res.error) {
          this.setState({Procced : true})
        } else {
          dispatch(ToastActions.ShowToast(Txt.ProblemWithConnection))
          this.setState({isLoading : false})
        }
      })
    } else {
      dispatch(ToastActions.ShowToast(' تمامی موارد الزامی باید تکمیل گردد '))
    }
  }
  //
  onImageChange = (event) => {
    let Profile = event.target.files
    if(Profile[0].type ==='image/jpeg'){
        if (Profile){
          if(Profile[0]) {
            this.setState({Profile : Profile});
            let reader = new FileReader();
            reader.onload = (e) => {
                this.setState({image: e.target.result , Profile : Profile});
            };
            reader.readAsDataURL(event.target.files[0]);
        }}
      } else {
        this.props.dispatch(ToastActions.ShowToast(Txt.OnlyJPG))
      }
    }

  render() {
    console.log(typeof this.state.NNumber)
    let EmailChecked = null
    let RNNChecked = null
    let LNNChecked = null
    // eslint-disable-next-line
    if(this.state.ChekingEmail == 'notChecked'){
      EmailChecked = <div className="icon__place"></div>
      // eslint-disable-next-line
    } else if(this.state.ChekingEmail == 'Checking'){
      EmailChecked = <div className="icon__place"><Icon loading name='spinner' /></div>
      // eslint-disable-next-line
    } else if(this.state.ChekingEmail == 'CheckedValid'){
      // eslint-disable-next-line
      EmailChecked = <div className="icon__place"><Icon  color='green' name='check circle' /></div>
      // eslint-disable-next-line
    } else if(this.state.ChekingEmail == 'CheckedNotValid'){
      // eslint-disable-next-line
      EmailChecked = <div className="icon__place"><Icon  color='red' name='times circle' /></div>
    }
    // eslint-disable-next-line
    if(this.state.ChekingRNN == 'notChecked'){
      RNNChecked = <div className="icon__place"></div>
      // eslint-disable-next-line
    } else if(this.state.ChekingRNN == 'Checking'){
      RNNChecked = <div className="icon__place"><Icon loading name='spinner' /></div>
      // eslint-disable-next-line
    } else if(this.state.ChekingRNN == 'CheckedValid'){
      // eslint-disable-next-line
      RNNChecked = <div className="icon__place"><Icon  color='green' name='check circle' /></div>
      // eslint-disable-next-line
    } else if(this.state.ChekingRNN == 'CheckedNotValid'){
      // eslint-disable-next-line
      RNNChecked = <div className="icon__place"><Icon  color='red' name='times circle' /></div>
    }
    // eslint-disable-next-line
    if(this.state.ChekingLNN == 'notChecked'){
      LNNChecked = <div className="icon__place"></div>
      // eslint-disable-next-line
    } else if(this.state.ChekingLNN == 'Checking'){
      LNNChecked = <div className="icon__place"><Icon loading name='spinner' /></div>
      // eslint-disable-next-line
    } else if(this.state.ChekingLNN == 'CheckedValid'){
      // eslint-disable-next-line
      LNNChecked = <div className="icon__place"><Icon  color='green' name='check circle' /></div>
      // eslint-disable-next-line
    } else if(this.state.ChekingLNN == 'CheckedNotValid'){
      // eslint-disable-next-line
      LNNChecked = <div className="icon__place"><Icon  color='red' name='times circle' /></div>
    }
    const panes = [
      { menuItem: ' شخص حقیقی ', render: () =>(
        <Tab.Pane>
          <div>
            <div className="row">
              <div className="col-sm-12 col-lg-12">
                <label 
                  htmlFor="firstname" 
                  className="label">نام
                  <span title="این فیلد اجباری است." className="form-required">*</span>
                </label>
                <input 
                  type="text"
                  onChange = {this.FirstnameOnChange}
                  required="required" 
                  id="firstname" 
                  name="firstname" 
                  placeholder="نام را وارد کنید..." 
                  className="required input mb-1  "/>
                  {!this.state.FirstNameValid &&
                        <span className="validate-error"> {Txt.EnterValidInput}</span>}
              </div>
              <div className="col-sm-12 col-lg-12">
                <label 
                  htmlFor="lastname" 
                  className="label">نام خانوادگی
                  <span title="این فیلد اجباری است." className="form-required">*</span>
                </label>
                <input 
                  type="text" 
                  onChange = {this.LastnameOnChange}
                  required="required" 
                  id="lastname" 
                  name="lastname" 
                  placeholder="نام خانوادگی را وارد کنید..." 
                  className="required input mb-1 "/>
                  {!this.state.LastNameValid &&
                        <span className="validate-error"> {Txt.EnterValidInput}</span>}
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12 col-lg-12">
                <label 
                  htmlFor="email" 
                  className="label">آدرس ایمیل
                  <span title="این فیلد اجباری است." className="form-required">*</span>
                </label>
                <input 
                  type="text" 
                  onChange = {this.EmailOnChange}
                  id="email" 
                  name="mail"
                  onBlur = {this.CheckForEmail}
                  placeholder="آدرس ایمیل را وارد کنید..." 
                  className="input mb-1 erorr"/>
                  {EmailChecked}
                  {!this.state.EmailValid &&
                        <span className="validate-error"> {Txt.EnterValidEmail}</span>}
              </div>
            </div>
            <div className="row">
              <div 
                className="col-sm-12 col-lg-6">
                <label 
                  htmlFor="birthday" 
                  className="label">تاریخ تولد
                </label>
                <input 
                  type="text" 
                  onChange = {this.BirthdateOnChange}
                  id="birthday" 
                  name="birthday" 
                  placeholder=" 1111-22-33  تاریخ تولد را وارد کنید..." 
                  className="input mb-1 erorr"/>
                  {!this.state.BirthDateValid &&
                        <span className="validate-error"> {Txt.EnterValidDate}</span>}
              </div>
              <div 
                className="col-sm-12 col-lg-6">
                <label 
                  htmlFor="id" 
                  className="label">شماره ملی
                  <span title="این فیلد اجباری است." className="form-required">*</span>
                </label>
                <input 
                  type="text" 
                  onChange = {this.NNumberOnChange}
                  id="id" 
                  name="id" 
                  onBlur = {this.CheckForRNN}
                  placeholder="شماره ملی را وارد کنید..." 
                  className="input mb-1 erorr"/>
                  {RNNChecked}
                  {!this.state.NNumberValid &&
                        <span className="validate-error"> {Txt.EnterNNumber}</span>}
              </div>
            </div>
            <div className="row">
              <div 
                className="col-sm-12 col-lg-12">
                <label 
                  htmlFor="company" 
                  className="label">نام شرکت یا سازمان محل کار(اختیاری)
                </label>
                <input 
                  type="text" 
                  onChange = {this.FirmnameOnChange}
                  id="company" 
                  name="company" 
                  placeholder="نام شرکت یا سازمان محل کار(اختیاری) را وارد کنید..." 
                  className="input mb-1  "/>
                  {!this.state.FirmNameValid &&
                        <span className="validate-error"> {Txt.EnterValidInput}</span>}
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12 col-lg-12">
                <label 
                  htmlFor="job" 
                  className="label">شغل(اختیاری)
                </label>
                <input 
                  type="text" 
                  onChange = {this.JobnameOnChange}
                  id="job" 
                  name="job" 
                  placeholder="شغل(اختیاری) را وارد کنید..." 
                  className="input mb-1  "/>
                  {!this.state.JobNameValid &&
                        <span className="validate-error"> {Txt.EnterValidInput}</span>}
              </div>
            </div>
            <div className="row">
              <div 
                className="col-sm-12 col-lg-6">
                <input
                  type="file"
                  onChange={this.onImageChange}
                  className="btn"
                  />
              </div>
              <div 
                className="col-sm-12 col-lg-6">
                  <center>
                  { this.state.image ?
                      <img
                        className="profile-pic"
                        src={this.state.image}
                        alt = {Txt.AltProp}
                        />
                    :
                      <i className="massive user circle icon"></i>}
                </center>
              </div>
            </div>
            <label className="label">
              <div 
                onClick   = { this.sendFinalReal}
                className = "green-btn btn">ثبت نام
                </div>
            </label>
          </div>
        </Tab.Pane>
        )},
      { menuItem: ' شخص حقوقی ', render: () =>(
        <Tab.Pane>
          <div>
            <div className="row">
              <div className="col-sm-12 col-lg-12">
                <label 
                  htmlFor="company_name" 
                  className="label">نام شرکت
                  <span title="این فیلد اجباری است." className="form-required">*</span>
                </label>
                <input 
                  type="text" 
                  onChange = {this.FirmnameOnChange}
                  required="required" 
                  id="company_name" 
                  name="company_name" 
                  placeholder="نام شرکت را وارد کنید..." 
                  className="required input mb-1 erorr"/>
                  {!this.state.FirmNameValid &&
                        <span className="validate-error"> {Txt.EnterValidInput}</span>}
              </div>
              <div className="col-sm-12 col-lg-12">
                <label 
                  htmlFor="_id" 
                  className="label">شماره ثبت
                  <span title="این فیلد اجباری است." className="form-required">*</span>
                </label>
                <input 
                  type="text"
                  onChange = {this.NSubmitOnChange}
                  required="required" 
                  id="_id" 
                  name="_id" 
                  placeholder="شماره ثبت را وارد کنید..." 
                  className="required input mb-1 erorr"/>
                  {!this.state.NSubmitValid &&
                        <span className="validate-error"> {Txt.EnterJustNumber}</span>}
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12 col-lg-6">
                <label 
                  htmlFor="id" 
                  className="label">شماره ملی
                  <span title="این فیلد اجباری است." className="form-required">*</span>
                </label>
                <input 
                  type="text" 
                  onChange= {this.NNumberLegalOnChange}
                  id="id" 
                  name="id" 
                  onBlur = {this.CheckForLNN}
                  placeholder="شماره ملی را وارد کنید..." 
                  className="input mb-1 erorr"/>
                  {LNNChecked}
              </div>
                {!this.state.NNumberLegalValid &&
                          <span className="validate-error"> {Txt.EnterJustNumber}</span>}

              <div className="col-sm-12 col-lg-6">
                <label 
                  htmlFor="est" 
                  className="label">تاریخ ثبت
                  <span title="این فیلد اجباری است." className="form-required">*</span>
                </label>
                <input 
                  type="text" 
                  onChange = {this.BirthdateOnChange}
                  id="est" 
                  name="est"
                  placeholder=" 1111-22-33  تاریخ ثبت را وارد کنید..." 
                  className="input mb-1 erorr"/>
                  {!this.state.BirthDateValid &&
                        <span className="validate-error"> {Txt.EnterValidDate}</span>}
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12 col-lg-12">
                <label 
                  htmlFor="founder" 
                  className="label">نام مدیرعامل
                  </label>
                <input 
                  type="text" 
                  onChange = {this.CEOOnChange}
                  id="founder" 
                  name="founder" 
                  placeholder="نام مدیرعامل را وارد کنید..." 
                  className="input mb-1  "/>
                  {!this.state.CEONameValid &&
                        <span className="validate-error"> {Txt.EnterValidInput}</span>}
              </div>
              <div className="col-sm-12 col-lg-12">
                <label 
                  htmlFor="email" 
                  className="label">آدرس ایمیل
                  <span title="این فیلد اجباری است." className="form-required">*</span>
                </label>
                <input 
                  type="text" 
                  onChange = {this.EmailOnChange}
                  id="email" 
                  name="mail" 
                  placeholder="آدرس ایمیل را وارد کنید..." 
                  onBlur = {this.CheckForEmail}
                  className="input mb-1 erorr"/>
                  {EmailChecked}
                  {!this.state.EmailValid &&
                        <span className="validate-error"> {Txt.EnterValidEmail}</span>}
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12 col-lg-12">
                <label 
                  htmlFor="field" 
                  className="label">حوزه فعالیت(اختیاری)
                </label>
                <input 
                  type="text"
                  value = {this.state.FieldName}
                  onChange = {this.FieldOnChange}
                  id="field" 
                  name="field" 
                  placeholder="حوزه فعالیت(اختیاری) را وارد کنید..." 
                  className="input mb-1  "/>
                  {!this.state.FieldNameValid &&
                        <span className="validate-error"> {Txt.EnterValidInput}</span>}
              </div>
            </div>
            <div className="row">
              <div 
                className="col-sm-12 col-lg-6">
                <input
                  type="file"
                  onChange={this.onImageChange}
                  className="btn"
                  />
              </div>
              <div 
                className="col-sm-12 col-lg-6">
                  <center>
                  { this.state.image ?
                      <img
                        className="profile-pic"
                        src={this.state.image}
                        alt = {Txt.AltProp}
                        />
                    :
                      <i className="massive user circle icon"></i>}
                  </center>
              </div>
            </div>
            <label className="label">
              <div 
                onClick   = { this.sendFinalLegal}
                className = "green-btn btn">ثبت نام
              </div>
            </label>
          </div>
        </Tab.Pane>
      )},
    ]
    return(
      <div className="container">
        {this.state.Procced && <Redirect to='/login'/>}
        {this.state.isLoading && <Loading /> }
        <div className="form-box">
        <h3>ثبت نام</h3>
          <Tab panes={panes} onTabChange = {this.resetTab} />
        </div>
      </div>
    )
  }
}
export default connect()(Final)