// Authors =  {logic : MJ SAM }
import React, { Component } from 'react'
import LoginComponent from './LoginComponent'
import {ResetPassword} from '..'

export default class Authentication extends Component {
  constructor(props){
    super(props)
    this.state = {
      Screen : 'Login',
    }
    this.Vars = {
      UserInfo : '',
      }
  }
  ChangeScreen = (Screen) => this.setState({Screen})
  updateUserInfo = (newUsrInfo) => {this.Vars.UserInfo = newUsrInfo }

  render() {
    let CurrentComponent = null
    switch (this.state.Screen) {
      case 'Login'  :
        CurrentComponent = (
            <LoginComponent
              close = {this.props.close}
              ChangeScreen = {this.ChangeScreen}
              />
        )
        break;
      
      case 'ResetPassword':
        CurrentComponent = (
            <ResetPassword
              ChangeScreen = {this.ChangeScreen}
              updateUserInfo = {this.updateUserInfo}
              />
        )
        break;
      default:
        CurrentComponent = (
            <LoginComponent
              AfterSuccessfullLogin = {this.props.AfterSuccessfullLogin}
              ChangeScreen = {this.ChangeScreen}
              />
        )
      }
    return(
      <div>
        {CurrentComponent}
      </div>
    )
  }
}
