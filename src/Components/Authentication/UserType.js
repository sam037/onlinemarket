import { Tab } from 'semantic-ui-react'

const panes = [
  { menuItem: ' شخص حقیقی ', render: () => <Tab.Pane>Tab 1 Content</Tab.Pane> },
  { menuItem: ' شخص حقوقی ', render: () => <Tab.Pane>Tab 3 Content</Tab.Pane> },
]

const TabExampleBasic = () => <Tab panes={panes} />

export default TabExampleBasic