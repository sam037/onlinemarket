// Authors =  {logic : MJ SAM }
import React, { Component } from 'react'
import {AuthActions, ToastActions} from '../../Redux/Actions'
import { connect } from 'react-redux'
import Txt from '../../Utils/txtConstant'
import { Form, Popup, Message,} from 'semantic-ui-react'

//import { authActionCreators } from '../../redux/authentication/authenticationActionCreator'

class SignupComponent extends Component {
  ///////////////// INITIALIZATION AND REACT COMPONENT /////////////////////////////
  constructor(props){
    super(props);
    this.Vars = {
      validPass : false,
      Email     : '',
      validEmail      : true,
      pass : '',
      repeatPass : '',
      corrected_repeat_pass : false,
      user : '',
    }
    this.state = {
      PassLengthValid : false ,
      PassCharValid : false ,
      PassStartValid : false ,
      PasswordErr : false,
      EmailErr : false,


      RepPassErr : false,
      isLoading : false,
      proceed : false,
      Update : false,
      Error : [],
      }
    }
    componentWillMount() {
    }
    componentDidUpdate(){
      if(this.state.proceed === true){
        this.props.ChangeScreen('VerifyPhone')
      }
    }
  ////////////////////// Functions ///////////////////////////////////////////////////////////
  setEmail = (event) => {
    this.Vars.Email = event.target.value.trim()
    this.validateEmail()
  }
  validateEmail = () => {
    var reEmail = /^[a-z][a-zA-Z0-9_.]*(\.[a-zA-Z][a-zA-Z0-9_.]*)?@[a-z][a-zA-Z-0-9]*\.[a-z]+(\.[a-z]+)?$/i;
    let validEmail = reEmail.test(this.Vars.Email)
    if(this.Vars.Email === '') validEmail = true
    this.Vars.validEmail = validEmail
    this.setState({
      EmailErr : !validEmail ,
    })
  }
  validatePassword = () => {
    var reLength = /^[ A-Za-z0-9_@.#&+-]{4,24}$/i;
    var reChar = /^[A-Za-z0-9_.]*$/i;
    var reStart = /^[a-zA-z][A-Za-z0-9_@.#&+-]*$/i;
    var reLengthValid = reLength.test(this.Vars.pass)
    var reCharValid = reChar.test(this.Vars.pass)
    var reStartValid = reStart.test(this.Vars.pass)

    this.setState({
      PassLengthValid : reLengthValid ,
      PassCharValid : reCharValid ,
      PassStartValid : reStartValid ,})

    if(reLengthValid && reCharValid && reStartValid){
        this.Vars.validPass = true
        this.setState({PasswordErr  : false })
        } else {
        this.Vars.validPass = false
        this.setState({PasswordErr  : true, isLoading : false})
    }
  }
  AskForSignUp = () => {
    const { dispatch } = this.props
    if(this.Vars.Email !== '' &&  this.Vars.pass !== '' && this.Vars.repeatPass !== ''){
      this.validatePassword()
      this.validateEmail();
      if(this.Vars.validPass && this.Vars.validEmail){
        this.setState({isLoading : true})
        if(this.Vars.corrected_repeat_pass){
          dispatch(AuthActions.signUp(this.Vars.Email, this.Vars.pass)).then(
            function(data){
                  const {error,payload} = data;
                  if(error) {
                    dispatch(ToastActions.ShowToast(payload.message))
                    this.setState({isLoading : false});
                  } else {
                    this.props.updateUserInfo(payload.user_info)
                    this.setState({proceed : true});
                  }
                }.bind(this)
              )
            }
      }
    }else {
      dispatch(ToastActions.ShowToast(Txt.All))
      this.setState({EmailErr : true , PasswordErr : true , RepPassErr : true})
    }
  }
  onChangeDoRepeatPass = (event) => {
    this.Vars.repeatPass = event.target.value
    if(event.target.value === this.Vars.pass && event.target.value !== '') {
        this.Vars.corrected_repeat_pass = true
        this.setState({RepPassErr : false })
      }
    else {
        this.Vars.corrected_repeat_pass = false
        this.setState({RepPassErr : true })
      }
    }
  onChangeDoPass = (event) => {
    this.Vars.pass = event.target.value
    this.validatePassword()
    if(event.target.value === this.Vars.repeatPass && event.target.value !== '') {
        this.Vars.corrected_repeat_pass = true
      }
    else {
        this.Vars.corrected_repeat_pass = false
      }
  }

  ColorSelector = (state) => {
    if(state){
      return 'green'
    } else {
      return 'orange'

    }
  }
  /////////////////////////////// RENDER Function //////////////////////////////////////////////
  render() {
    return (
      <div className = "ui raised very padded segment container">
        <div className="ui piled segments">
          <div className="ui teal segment">
            <h2 className="ui center aligned teal header">
              {Txt.Signup}
            </h2>
          </div>
          <div className="ui segment">
            <div className="ui two column middle aligned very relaxed stackable grid">
              {this.state.isLoading
                &&
              <div className="ui active dimmer">
                <div className="ui indeterminate text loader">{Txt.ConnectingToServer}</div>
              </div>}
              <div className="column center">
                <div className="ui teal piled segment">
                  <Form warning = {this.state.Error.length !== 0} >
                    <Form.Input
                      icon='email'
                      iconPosition='left'
                      placeholder= {Txt.Email}
                      onChange={this.setEmail}
                      error = {this.state.EmailErr}
                      onBlur = {this.validateEmail}
                      />
                      <Popup
                        position = "bottom left"
                        trigger={
                          <Form.Input
                            icon='key'
                            iconPosition='left'
                            placeholder = {Txt.PassPlace}
                            onChange={this.onChangeDoPass}
                            type = 'password'
                            error = {this.state.PasswordErr && this.Vars.pass !== ''}
                            onBlur = {this.validatePassword}
                          />
                        }
                        on='focus'
                      >
                        <Message size='mini' color = {this.ColorSelector(this.state.PassLengthValid)} >
                          {Txt.PassLength}
                        </Message>
                        <Message size='mini' color = {this.ColorSelector(this.state.PassCharValid)} >
                          {Txt.PassChar}
                        </Message>
                        <Message size='mini' color = {this.ColorSelector(this.state.PassStartValid)} >
                          {Txt.PassStart}
                        </Message>
                      </Popup>
                      <Form.Input
                        icon='copy'
                        iconPosition='left'
                        placeholder = {Txt.RePPlace}
                        onChange={this.onChangeDoRepeatPass}
                        type = 'password'
                        error = {this.state.RepPassErr}
                      />
                      <button
                        className = "ui teal basic button fluid"
                        onClick = {() => this.AskForSignUp()}>
                        <p > ثبت نام</p>
                      </button>
                      <Message
                        warning >
                        {this.state.Error}
                      </Message>
                    </Form>
                </div>
              </div>
              <div className="center aligned column">
                  <i className="user plus icon teal massive"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default connect()(SignupComponent)
