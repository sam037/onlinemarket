// Author = MJ SAM
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { AuthActions , ToastActions } from '../../Redux/Actions'
import Txt from '../../Utils/txtConstant'
import { Redirect } from 'react-router';
import {  Loading } from '..'
class SubmitResetPassword extends Component {
    constructor(props){
        super(props)
        this.state = {
          isLoading   : true ,

          Proceed     : false,
          Pass        : '',
          PassValid : false,
          PassLengthValid   : true,
          PassCharValid     : true,
          PassStartValid    : true,

          RepPass         : '',
          RepPassValid  : true,
        }
      //(this.props.match.params.resetcode)

    }
    PassOnChange = (event) => {
      let pass = event.target.value.trim()
      var reLength = /^[ A-Za-z0-9_@.#&+-]{4,24}$/i;
      var reChar = /^[A-Za-z0-9_.]*$/i;
      var reStart = /^[a-zA-z][A-Za-z0-9_@.#&+-]*$/i;
      var reLengthValid = reLength.test(pass)
      var reCharValid = reChar.test(pass)
      var reStartValid = reStart.test(pass)
      // eslint-disable-next-line
      let RepPassValid = (this.state.RepPass == pass)
      if(reLengthValid && reCharValid && reStartValid){
          this.setState({
              Pass                : pass,
              PassValid           : true,
              RepPassValid      : RepPassValid,
              PassLengthValid   : reLengthValid,
              PassCharValid     : reCharValid,
              PassStartValid    : reStartValid,
              })
        // eslint-disable-next-line
      } else if(pass == ''){
          this.setState({
              PassLengthValid   : true,
              PassCharValid     : true,
              PassStartValid    : true,
              })
      } else {
          this.setState({
              RepPassValid      : RepPassValid,
              PassLengthValid   : reLengthValid,
              PassCharValid     : reCharValid,
              PassStartValid    : reStartValid,
              })
      }
  }
  RepPassOnChange = (event) => {
      let repeatPass = event.target.value
      // eslint-disable-next-line
      if(repeatPass === this.state.Pass && event.target.value != '') {
          this.setState({
              RepPass : repeatPass,
              RepPassValid : true 
          })
      }
      else {
          this.setState({
              RepPass : repeatPass,
              RepPassValid : false
          })
      }
  }
  SubmitPassword = () => {
    const { dispatch } = this.props
    const { PassValid , RepPassValid } = this.state
    if(PassValid && RepPassValid){
      this.setState({isLoading : true})
      dispatch(AuthActions.SubmitNewPassword(this.props.resetcode,this.state.Pass)).then( (data) =>{
          const {error} = data;
          console.log(data)
          if(error) {
            dispatch(ToastActions.ShowToast(' لینک معتبر نیست '))
          } else  { 
            dispatch(ToastActions.ShowToast(' کلمه عبور جایگزاری شد '))
            this.setState({Proceed : true})
          }
        })
    }
  }

  componentDidMount(){
    console.log(this.props.resetcode)
    const { dispatch } = this.props
    dispatch(AuthActions.CheckValidityTocken(this.props.resetcode)).then((res)=>{
      if(res.error){
        console.log(res)
        dispatch(ToastActions.ShowToast(' لینک معتبر نیست '))
        this.setState({Proceed : true})
      } else {
        this.setState({isLoading : false})
      }
    })
  }

  render() {
    return (
      <div className="container">
        {this.state.isLoading && <Loading />}
        {this.state.Proceed && <Redirect to='/' />}
        <div className="form-box">
        <label className="label">
          <h3> بازنشانی رمز عبور </h3>
        </label>
        <div className="row">
            <div className="col-sm-12 col-lg-6">
                <label htmlFor="password" className="label">کلمه عبور<span title="این فیلد اجباری است." className="form-required">*</span></label>
                <input 
                    type="password" 
                    onChange = {this.PassOnChange}
                    required="required" 
                    id="password" 
                    name="password" 
                    placeholder="کلمه عبور را وارد کنید..." 
                    className="required input mb-1 erorr"/>
                    
                    {!this.state.PassLengthValid &&
                        <span className="validate-error"> {Txt.PassLength}</span>}
                    {!this.state.PassCharValid && 
                        <span className="validate-error"> {Txt.PassChar}</span>}
                    {!this.state.PassStartValid && 
                        <span className="validate-error"> {Txt.PassStart}</span>}
            </div>
            <div className="col-sm-12 col-lg-6">
                <label 
                    htmlFor="confirm_password" 
                    className="label">تکرار عبور
                    <span 
                        title="این فیلد اجباری است." 
                        className="form-required">*
                    </span>
                </label>
                <input 
                    type="password" 
                    onChange = {this.RepPassOnChange}
                    required="required" 
                    id="confirm_password" 
                    name="confirm_password" 
                    placeholder="تکرار عبور را وارد کنید..." 
                    className="required input mb-1 erorr"/>
                {!this.state.RepPassValid && 
                        <span className="validate-error"> {Txt.RepPMustBeSame}</span>}
              </div>
            </div>    
            <label className="label">
                <button 
                    className="green-btn btn" onClick={this.SubmitPassword}>ثبت نام</button>
            </label>
        </div>
      </div>
    )
  }
}

export default connect()(SubmitResetPassword)
