import React, { Component } from 'react'
import 'semantic-ui-css/semantic.min.css';
import { Search,} from 'semantic-ui-react'
import { SearchActions } from '../../Redux/Actions'
import { connect } from 'react-redux'
import { Link,Redirect } from 'react-router-dom'
import {Txt } from '../../Utils'
import {ProductRender } from '..'

const resultRenderer = (item) =>{
  return  <ProductRender Item = {item} />
 }

class SearchBar extends Component {
  constructor(){
    super()
    this.state = {
      isLoading : false,
      value     : '',
      results   : [],
      Redirect : false
    }
  }
  handleResultSelect = (e, { result }) => {
    this.setState({ value: result.title })
  }
  handleSearchChange = (e, { value }) => {
    this.setState({ isLoading: true, value })
    this.props.dispatch(SearchActions.Basic(value)).then( (response) =>  {this.setState({
                                                            isLoading: false,
                                                            results: response.payload.product
                                                          })
                                                        })
  }
  handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      this.setState({Redirect:true})
    }
  }
  render() {
    const { isLoading, value, results } = this.state
    return (
      <div style = {{marginRight : '10%' ,marginLeft : '10%',marginTop : '100px'}} >
        {this.state.Redirect ? <Redirect to='/search' /> : null}
        <Search
          input             = {{ fluid: true }}
          loading           = {isLoading}
          onKeyPress        = {this.handleKeyPress}
          resultRenderer    = {resultRenderer}
          onResultSelect    = {this.handleResultSelect}
          onSearchChange    = {this.handleSearchChange}
          results           = {results}
          value             = {value}
        />
        <Link to = {'/Search'} > {Txt.AdvanceSearch} </Link>
      </div>
    )
  }
}
export default connect()(SearchBar)
