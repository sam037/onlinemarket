import React, { Component } from 'react';
import { Link } from 'react-router-dom'
//import '../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import {connect } from 'react-redux'
import { SearchBar } from '../../Components'
class BriefIntroduction extends Component {
    render() {
        return (
        <div className="container-fluid layer section" >
            <div className="row middle-xs" >
            <div className="col-xs-12 col-sm-12 col-md-3 section__body" >
                <h5 className="slogan company--name" style={{fontFamily:'shabnam'}}>
                    فروشگاه صنعت آموز، شرکتی در حوزه تامین منابع آموزشی 
                </h5>
                <h1 className="title-first company--name" style={{fontFamily:'shabnam'}}>
                    تجربه بهترین خدمات آموزشی
                </h1>
                <h3 className="title-second company--name" style={{fontFamily:'shabnam'}}>
                    به باشگاه مشتریان صنعت آموز بپیوندید تا با نرخ‌های ویژه و تسهیلات از خدمات آموزشی برخوردار شوید.
                </h3>
                {!this.props.UsrID ? <Link to='/signup' className="btn btn-yellow mb-1">به ما بپیوندید</Link> : ''}
            </div>
            <div className="col-xs-12 col-sm-12 col-md-9 first-xs last-sm section__body">
                <SearchBar />
            </div>
            </div>
        </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {  UsrID: state.Authentication.user_id,};
  };

export default connect(mapStateToProps)(BriefIntroduction)