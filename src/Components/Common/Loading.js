import React, { Component } from 'react';

//import '../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

class Loading extends Component {
    render() {
        return (
            <div className="cube-wrapper">
                <div className="cube-folding"><span className="leaf1"></span><span className="leaf2"></span><span className="leaf3"></span><span className="leaf4"></span></div><span data-name="Loading" className="loading">صبرکنید</span>
            </div>
        );
    }
}


export default Loading

/*
<div className="loader-comp">
                <div className="loader">
                <div className="spinner yellow"></div>
                <div className="spinner orange"></div>
                <div className="spinner red"></div>
                <div className="spinner pink"></div>
                <div className="spinner violet"></div>
                <div className="spinner mauve"></div>
                <div className="spinner light-yellow"></div>
                </div>
            </div>

*/