import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Modal ,Menu} from 'semantic-ui-react'
import { AuthActions,} from '../../Redux/Actions'
import { Link } from 'react-router-dom'
import { Authentication } from '..'
//import {Txt } from '../../Utils'
import './styles.css'

class FixedMenu extends Component {
  constructor(props){
    super(props)
    this.state = {
      modalOpen : false,
      DashBoard : false,
    }
  }
  toggleSidebar = ()=>{
    const { dispatch } = this.props
    dispatch({
        type : 'TOGGLE_SIDEBAR',
      })
  }
  render() {
    return (
      <div>
        <Menu
          fixed={this.props.fixed ? 'top' : null}
          inverted={!this.props.fixed}
          pointing={!this.props.fixed}
          secondary={!this.props.fixed} >
          <div className="ui container" >
            <div className="ui  listitem block" style = {{width : '100%'}} id = "fixedMenuContainer">
              <span style = {{float: 'right'}}>
                <button className="ui circular green icon basic button" onClick ={this.toggleSidebar}>
                  <i className="bars icon"></i>
                </button>
                <Link to = {`/`}>
                  <button className="ui circular green icon basic button">
                    <i className="home icon"></i>
                  </button>
                </Link>
              </span>
              <span style = {{float: 'left'}}>
                {this.props.UsrID ?
                  <div>
                    <button
                      className="ui circular icon basic red button"
                      onClick = {() => this.props.dispatch(AuthActions.SignOut())}>
                      <i className="sign out alternate icon"></i>
                    </button>
                  </div>
                  :
                  <button
                    className="ui circular basic icon blue button"
                    onClick = {() => this.setState({modalOpen : true})}
                    >
                    <i className="sign in alternate icon"></i>
                  </button>
                }
              </span>
            </div>
        </div>
      </Menu>
      <Modal
        open                  = {this.state.modalOpen}
        onClose               = {() => this.setState({modalOpen : false}) }
        dimmer                = {false}
        closeOnRootNodeClick  = {false}
        closeIcon
        >
        <Modal.Content>
          <Authentication AfterSuccessfullLogin = {() => this.setState({modalOpen : false})}/>
        </Modal.Content>
      </Modal>
    </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    UsrID   : state.Authentication.user_id ,
    Sidebar : state.Toast.sidebar }
}

export default connect(mapStateToProps)(FixedMenu);
