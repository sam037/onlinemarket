import React, { Component } from 'react'


export default class Timer extends Component {
  constructor (props) {
    super(props)
    this.state = {count: this.props.seconds}
  }
  componentWillMount () {
    clearInterval(this.timer)
    this.timer = setInterval(this.tick , 1000)
  }
  tick = () => {
    if(this.state.count >= 1) {
      const Next = this.state.count - 1
      this.setState({count: Next})

    } else {
      clearInterval(this.timer)
      this.props.AfterTimer()
    }
  }

  render () {
    return (
      <div className="ui small statistic">
         {this.props.Text ? this.props.Text : ''}{this.state.count}
      </div>
    )
  }
}
