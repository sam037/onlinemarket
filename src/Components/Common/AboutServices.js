import React, { Component } from 'react';

//import '../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css';



class AboutServices extends Component {

    //bootstrapURLKeys={{ key: /* YOUR KEY HERE */ }}
    render() {
        return (
            <div className="container-fluid section">
                <div className="row">
                    <div className="col-xs-12 col-md-5 content col-md-offset-1 section__body">
                    <div className="main-title">
                        <div className="title" style={{fontFamily:'shabnam'}}><b>درباره ما</b></div>
                    </div>
                    <div className="paragraph">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. </div>
                    <div className="row">
                        <div className="col-xs-12 col-sm-6 col-md-6 section__body">
                        <div className="item">
                            <img src="./dist/images/svg/strongbox.svg" alt="" className="icon"/>
                            <div className="text">
                            <div className="title">پرداخت امن</div>
                            <p>تضمین بهترین قیمت و پرداختی امن از طرف آرتام به شما</p>
                            </div>
                        </div>
                        </div>
                        <div className="col-xs-12 col-sm-6 col-md-6 section__body">
                        <div className="item">
                            <img src="./dist/images/svg/24-hours.svg" alt="" className="icon"/>
                            <div className="text">
                            <div className="title">پشتیبانی ۲۴ ساعته</div>
                            <p>هر لحظه از شبانه روز ما پشتیبان شما خواهیم بود</p>
                            </div>
                        </div>
                        </div>
                        <div className="col-xs-12 col-sm-6 col-md-6 section__body">
                        <div className="item">
                        <img src="./dist/images/svg/medal.svg" alt="" className="icon"/>
                            <div className="text">
                            <div className="title">نماد اعتماد</div>
                            <p>دارنده نماد اعتماد از وزارت ارشاد و صنعت و معدن</p>
                            </div>
                        </div>
                        </div>
                        <div className="col-xs-12 col-sm-6 col-md-6 section__body">
                        <div className="item">
                        <img src="./dist/images/svg/listing.svg" alt="" className="icon"/>
                            <div className="text">
                            <div className="title">بروز و دقیق</div>
                            <p>هر لحظه از شبانه روز ما پشتیبان شما خواهیم بود</p>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div className="col-xs-12 col-md-6 section__body">
                    <img src="./dist/images/about.jpg" alt="" className="about--pic" /></div>
                </div>
            </div>
        );
    }
}


export default AboutServices