import React, { Component } from 'react';
import { Link } from 'react-router-dom'
class Footer extends Component {
    render() {
        return (
            <section className="footer">
                <div className="container">
                    <div className="first-footer">
                    <div className="row">
                        <div className="col-xs-12 col-sm-8">
                        <div className="logo">
                            <img src="../../dist/images/svg/logo.svg" alt=""/>
                        </div>
                        <div className="module-title">فروشگاه صنعت آموز</div>
                        <ul className="inline">
                            <li><Link to='/aboutus' >درباره ما</Link></li>
                            <li><Link to='/founders' >بنیان‌گذاران</Link></li>
                            <li><Link to='/signup' >به ما بپیوندید</Link></li>
                        </ul>
                        </div>
                        <div className="col-xs-12 col-sm-4">
                        <div className="second-footer">
                            <div className="module-title">اطلاعات تماس</div>
                            <div className="contact-info"><i className="icon-location"></i>
                            <div className="information">تهران، ضلع شمال غربی پل میدان ونک خیابان طاهرمظلومی، ساختمان ۳۹، طبقه دوم</div>
                            </div>
                            <div className="contact-info"><i className="icon-phone"></i>
                            <div className="information">پشتیبانی: ۳۴۶۵۲(۰۲۱)</div>
                            </div>
                            <div className="contact-info"><i className="icon-email"></i>
                            <div className="information">info@artam.com</div>
                            </div>
                        </div>
                        <div className="socials">
                            <a  className="social">
                                <i className="icon-telegram social-telegram"></i>
                                <div className="name">تلگرام</div>
                            </a>
                            <a  className="social">
                                <i className="icon-instagram social-instagram"></i>
                                <div className="name">اینستاگرام</div>
                            </a>
                            <a  className="social"><i className="icon-twitter social-twitter"></i>
                                <div className="name">توییتر</div>
                            </a>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="zebra" >
                <h4 style={{fontFamily:'shabnam'}}>کپی‌رایت ۱۳۹۵-۱۳۹۷، <a >فروشگاه صنعت آموز</a>.</h4>
                <p>تمامی خدمات این وب سایت دارای مجوزهای لازم از مراجع مربوطه می باشد و فعالیت های این سایت تابع قوانین و مقررات جمهوری اسلامی ایران است.</p>
            </div>
        </section>
        );
    }
}


export default Footer