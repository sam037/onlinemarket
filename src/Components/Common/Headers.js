import React, { Component } from 'react';
import { AuthActions, } from '../../Redux/Actions'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { Modal ,} from 'semantic-ui-react'
import 'semantic-ui-css/semantic.min.css';
import { Authentication } from '..'
//marginTop: '0px !important',
const inlineStyle = {
    modal : {
      marginLeft: 'auto',
      marginRight: 'auto',
      marginTop: 'auto !important',
      display: 'inline-block !important',
      position: 'relative',
      top: '20%',
    }
  };


class Headers extends Component {
    constructor(props){
        super(props)
        this.state = {
            GoSignUp    : false,
            GoLogin     : this.props.OpenLogin,
            MenuOpen    : false,
        }
    }
    
    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }
    setWrapperRef = (node) => {
        this.wrapperRef = node;
    }
    handleClickOutside = (event) => {
        if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
          this.Collapse()
        }
    }
    
    Collapse = () => {
        this.setState({MenuOpen : false})
    }
    SignOut = () => {
        this.props.dispatch(AuthActions.SignOut())
    }
    render() {
        return (
            <header 
                className={`main-header static header${this.state.MenuOpen ? ' menu-opened' : ''}`}
                ref={this.setWrapperRef}>
                <div className="container-fluid">
                    <div className="header--layout row">
                    <div className="col-xs-10 col-sm-4">
                        <Link to = '/' className="logo">
                            <img src="../../dist/images/svg/logo.svg" alt="logo"/>
                            <span className="company--name">شرکت توسعه منابع آموزشی </span>
                        </Link>
                    </div>
                    <div className="col-xs-2 col-sm-4 align-center" >
                        <nav className="header--menu">
                            {this.props.UsrID && <Link to = '/cart' >سبد خرید</Link>}
                            {this.props.UsrID && <Link to = '/buy' >ثبت شده ها</Link>}
                            {this.props.signup && this.state.MenuOpen && (this.props.UsrID ? (
                            <Link to='/'>
                                <a 
                                    className="btn btn-blueGreen" 
                                    onClick={this.SignOut} >خروج
                                </a>
                            </Link>
                            ):(
                            <div>
                                <Link to = '/signup' > ثبت نام </Link>
                                <a onClick = {()=> this.setState({GoLogin : true})}>ورود </a>
                            </div>
                        ))}
                        </nav>
                        <div className="burger-container" onClick={()=>this.setState({MenuOpen : true})}>
                            <div id="burger">
                                <div className="bar topBar"></div>
                                <div className="bar btmBar"></div>
                            </div>
                        </div>
                        
                    </div>
                    <div className="col-xs-2 col-sm-3 align-left hide-xs">
                        {this.props.signup &&(this.props.UsrID ? (
                            <Link to='/'>
                                <button 
                                    className="btn btn-blueGreen" 
                                    onClick={this.SignOut} >خروج
                                </button>
                            </Link>
                            ):(
                            <span>
                                <Link to='/signup'>
                                    <button  className="btn btn-blueGreen" >ثبت نام </button>
                                </Link>
                                &nbsp;
                                <Modal 
                                    open={this.state.GoLogin} 
                                    onClose={()=>this.setState({GoLogin : false})} 
                                    basic size='small'
                                    style={inlineStyle.modal}
                                    trigger={
                                    <button className="btn btn-blueGreen" onClick = {()=> this.setState({GoLogin : true})} >
                                            ورود
                                    </button>}
                                    closeIcon
                                    >
                                    <Modal.Content>
                                    <div className='ui container' style={{textAlign:'right',direction:'rtl'}}>
                                        <Authentication close = {()=>this.setState({GoLogin : false})} />
                                    </div>
                                    </Modal.Content>
                                </Modal>
                            </span>
                        ))}
                    </div>
                    </div>
                </div>
            </header>

        );
    }
}


const mapStateToProps = (state) => {
    return {  UsrID: state.Authentication.access_key };
  };
  
export default connect(mapStateToProps)(Headers)


//onClick={() => this.setState({GoLogin : true })}
//className="show-xs">به ما بپیوندید</a>