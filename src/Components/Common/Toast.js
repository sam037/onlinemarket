import React, { Component } from 'react'
import {Transition, } from 'semantic-ui-react'
import { connect } from 'react-redux'
import './styles.css'

class Toast extends Component {
  constructor(props){
    super(props)
    this.state = {
      visible : this.props.visible,
      content : this.props.content,
    }
  }
  componentWillReceiveProps(nextProps){
    if(nextProps.visible !== this.props.visible){
      if(nextProps.visible === true){
        this.setState({
          visible : nextProps.visible,
          content : nextProps.content.content,
        })
      } else {
        this.setState({
          visible : false,
          content : '',
        })
      }
    }
  }
  render() {
    return (
    <Transition
      visible={this.state.visible}
      animation='scale'
      duration={450}
      style ={{position : 'absulute' , zIndex : 2000}} >
      <div className="toastContainer" >
        <div className="toast" onClick={()=> this.setState({visible : !this.state.visible})}>
          {this.state.content}
        </div>
      </div>
    </Transition>
    );
  }
}

const mapStatetoProps = (state) => {
  return({
    visible : state.Toast.visible,
    content : state.Toast.content,
  })
}
export default connect(mapStatetoProps)(Toast)
