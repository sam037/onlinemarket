/*
Author : MJS
*/
import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Txt } from '../../Utils'
import { ProductActions } from '../../Redux/Actions'
import { TextArea, Input, Button } from 'semantic-ui-react';
import Dropzone from 'react-dropzone';
import { Checkbox } from 'semantic-ui-react'
import { Redirect } from 'react-router'

import _ from 'lodash'



class Admin extends Component {
  constructor(props){
    super(props);
    this.state = {
        Data : '',
        ProductLoading : true,
        User : '',
        FileToSent : '',
        Thumbnail : '',
      }
    this.Vars = {
      title : '',
      describtion : '',
      price : '',
      category : new Set([]),
    }
    let Category = [
      {key : 'ENG', value : ' فنی و مهندسی '    },
      {key : 'HWE', value : ' مهندس سخت افزار ' },
      {key : 'SWE', value : ' مهندسی نرم افزار '},
      {key : 'IT' , value : ' مهندسی شبکه '     },
      {key : 'AI' , value : ' هوش مصنوعی '      },
      ]
    this.RenderCategory(Category)

  }
  onDrop(acceptedFiles, rejectedFiles) {
    let isFilesValid = true
    if(acceptedFiles[0].type !== "application/pdf"){
        isFilesValid = false }
    if(isFilesValid)
       {
        this.setState({FileToSent : acceptedFiles[0]})
        alert(" فایل انتخاب شد ")
        }
  }
  onDrop2(acceptedFiles, rejectedFiles) {
    this.setState({Thumbnail : acceptedFiles[0]})
    console.log(acceptedFiles[0])
    alert(" فایل انتخاب شد ")
  }
  setTitle = (event) => this.Vars.title = event.target.value
  setDescribtion = (event) => this.Vars.describtion = event.target.value
  setPrice = (event) => this.Vars.price = event.target.value
  AddProduct = () =>{
    //console.log(this.state.FileToSent);
    //console.log(this.state.Thumbnail);
    this.props.dispatch(ProductActions.addProduct(
      this.Vars.title,
      this.Vars.describtion,
      this.Vars.price,
      Array.from(this.Vars.category),
      this.state.FileToSent,
      this.state.Thumbnail,
      )).then(
          function(response){
              if(response.status === 200 ){
                alert(" با موفقیت ارسال شد ")
              } else {
                alert(" نا موفق ")
              }
          }
      )
  }

  CheckHandler = (data,element) => {
    if(data.checked){
      this.Vars.category.add(element)
    } else {
      this.Vars.category.delete(element)
    }
  }
  RenderCategory = (CategoryArr) => {
    let Cnt = 0
    let CategoryComponents = []
    CategoryArr.forEach( (Category) => {
      CategoryComponents.push(
          <div className="ui row" key = {Cnt}>
            <div className="column">
              {Category.value}
            </div>
            <div className="column">
              <Checkbox  onClick = {(event,data) => this.CheckHandler(data,Category.key)}/>
            </div>
          </div>
        )
      Cnt = Cnt + 1
      }
    )
    this.CategoryComponents = CategoryComponents
  }
  render() {
    console.log(this.props);
    if(! _.includes(this.props.Roles,"admin")){
      return(<Redirect to='/'/>)
    }
    return(
      <center>
        <div>
          <Dropzone onDrop={(files) => this.onDrop(files)} >
            <div className="ui icon message fluid">
              <i className="inbox icon"></i>
              <div className="content">
                <div className="header">
                  {Txt.PutYourFileHere}
                </div>
              </div>
            </div>
          </Dropzone>
          <br/>
          <Dropzone onDrop={(files) => this.onDrop2(files)} >
            <div className="ui icon message fluid">
              <i className="inbox icon"></i>
              <div className="content">
                <div className="header">
                  put your thumbnail here
                </div>
              </div>
            </div>
          </Dropzone>
          <div className="ui equal width grid littleMargintop">
            {this.CategoryComponents}
          </div>
          <TextArea 
            placeholder='Title' 
            style={{ minHeight: 100 }} 
            onChange = {this.setTitle} /><br/>
          <TextArea 
            placeholder='Describtion' 
            style={{ minHeight: 100 }} 
            onChange = {this.setDescribtion} /><br/>
          <Input 
            placeholder = 'price' 
            onChange = {this.setPrice}/><br/><br/>
          <br/>
          <Button onClick={this.AddProduct}>
            {' اضافه کردن '}
          </Button><br/>
        </div>
      </center>
    )
  }
}
const mapStateToProps = (state) => {
  console.log(state)
  return {  UsrID   : state.Authentication.user_id,
            Roles   : state.Authentication.roles,
            Profile : state.User.profile}
}
export default connect(mapStateToProps)(Admin)
