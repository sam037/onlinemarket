import FixedMenu        from './Common/FixedMenu'
import Toast            from './Common/Toast'
import SearchBar        from './Common/SearchBar'
import Footer           from './Common/Footer'
import Headers		      from './Common/Headers'
import Loading 		from './Common/Loading'
import AboutServices    from './Common/AboutServices'
import BriefIntroduction from './Common/BriefIntroduction'
import Timer from './Common/Timer'
//import SearchBar        from './Common/SearchBar'
//import SearchBar        from './Common/SearchBar'

import Authentication from './Authentication/Authentication'
import LoginComponent from './Authentication/LoginComponent'
import SignupComponent from './Authentication/SignUpComponent'
import VerifyComponent from './Authentication/VerifyComponent'
import Final from './Authentication/Final'
import ResetPassword from './Authentication/ResetpasswordComponent'
import SubmitResetPassword from './Authentication/SubmitResetPassword'


import ProductList      from './Product/ProductList'
import ProductDetail    from './Product/ProductDetail'
import ProductDetailCart    from './Product/ProductDetailCart'
import ProductDetailBuy    from './Product/ProductDetailBuy'

import ProductRender    from './Product/ProductRender'

import ProductCard      from './Product/ProductCard'
import ProductCardCart      from './Product/ProductCardCart'
import ProductCardBuy      from './Product/ProductCardBuy'

import ProductTop       from './Product/ProductTop'
//import ProductRender    from './Product/ProductRender'

import ProfileView      from './User/ProfileView'
import AddBallance      from './User/AddBallance'
//import AddBallance      from './User/AddBallance'
//import AddBallance      from './User/AddBallance'

import Search           from './Search/Search'
import SearchResult     from './Search/SearchResult'
import SearchSide       from './Search/SearchSide'
//import Search           from './Search/Search'

import Administrator from './Admin/Admin'
export {
  FixedMenu,
  Toast,
  SearchBar,
  Footer,
  Headers,
  AboutServices,

  Loading,
  Timer,
  BriefIntroduction,

  Authentication,
  LoginComponent,
  SignupComponent,
  VerifyComponent,
  Final,
  ResetPassword,
  SubmitResetPassword,

  ProductList,
  ProductTop,
  ProductDetail,
  ProductDetailCart,
  ProductDetailBuy,
  ProductRender,
  ProductCard,
  ProductCardCart,
  ProductCardBuy,

  ProfileView,
  AddBallance,

  Search,
  SearchResult,
  SearchSide,

  Administrator,
}
