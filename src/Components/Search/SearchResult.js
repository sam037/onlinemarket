
import React, { Component } from 'react';
import { ProductCard} from '..'
import { SearchActions } from '../../Redux/Actions'
import { connect } from 'react-redux'
import { Card,} from 'semantic-ui-react'

class SearchResult extends Component {
  constructor(props){
    super(props)
    this.state = {
      isLoading : 'loading',
      Data : []
    }
    this.FetchedRenderProduct()
  }
  FetchedRenderProduct = () =>{
    this.setState({isLoading : 'loading' })
    let urlToget = `category=${this.props.Category}&keyword=${this.props.Keyword}`
    this.props.dispatch(SearchActions.Advance(urlToget)).then(
      function(data){
            const {error,} = data;
            if(error){
                this.setState({error : true})
            } else {
                const Data = data.payload.product
                this.Components = [];
                let Key = 0
                Data.forEach( (product) => {
                    Key = Key + 1
                    this.Components.push(<ProductCard key = {Key} Product = {product} />)
                  }
                )
                this.setState({isLoading : '' })
            }
          }.bind(this)
        )
  }
  componentDidUpdate(newProps){
    if(this.props.Keyword !== newProps.Keyword || this.props.Category !== newProps.Category ){
      this.setState({isLoading : true},this.FetchedRenderProduct())
    }
  }
  render() {
  return (
    <div className = "ui container mainProductList" >
      <Card.Group itemsPerRow={3}  className="rightToleft">
        {this.Components}
      </Card.Group>
    </div>
      )
  }
}

export default connect()(SearchResult)
