import React, { Component } from 'react';
import { Checkbox } from 'semantic-ui-react'
import { Txt, } from '../../Utils'

class SearchSide extends Component {
  constructor(props){
    super(props)
    this.Category = new Set([])
    this.RenderCategory(this.props.data.Category)
  }
  RequestCreator = () => {
    let ItrCategory = this.Category.values();
    let StrCategory = ''
    while (true) {
      let condition = ItrCategory.next()
      if(condition.done){
          break;
      }else {
        StrCategory = StrCategory + condition.value + ','
      }
    }
    // making str request
    let Request = `${StrCategory}`
    this.props.UpdateFilter(Request)
  }
  //////////////////
  CheckHandler = (data,element) => {

    if(data.checked){
      this.Category.add(element)
    } else {
      this.Category.delete(element)
    }
    this.RequestCreator()
  }
  //////////////////
  RenderCategory = (CategoryArr) => {
    let Cnt = 0
    let CategoryComponentsName = []
    let CategoryComponentsBox = []
    CategoryArr.forEach( (Category) => {
      CategoryComponentsName.push(
        <div className="column" key = {Cnt}>
          <center>
            {Category.value}
          </center>
        </div>
        )
      CategoryComponentsBox.push(
        <div className="column" key = {Cnt}>
          <center>
            <Checkbox  onClick = {(event,data) => this.CheckHandler(data,Category.key)}/>
          </center>
        </div>
      )
      Cnt = Cnt + 1
      }
    )
    this.CategoryComponentsName = CategoryComponentsName
    this.CategoryComponentsBox = CategoryComponentsBox
  }

  //////////////
  render() {
    return (
      <div className="ui teal segment rightToleft">
        <div className="sidebarHead" >
          {Txt.Category}
        </div>
        <div className="ui equal width grid littleMargintop">
          <div className="ui row" >{this.CategoryComponentsName}</div>
          <div className="ui row" >{this.CategoryComponentsBox}</div>
        </div>
      </div>
    );
  }
}

export default SearchSide
