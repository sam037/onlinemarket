import React, { Component } from 'react';
import {SearchSide , SearchResult, } from '..'

class Search extends Component {
  constructor(props){
    super(props)
    this.state = {
      Category : '',
      Keyword  : ''
    }
  }
  UpdateFilter = (newReq) => {
    this.setState({Category : newReq})
  }
  componentDidUpdate(nextProps){
    if(nextProps.keyword !== this.props.keyword){
      this.setState({Keyword : this.props.keyword })
    }
  }

  render() {
    let catdata = {
      Category : [
        {key : 'ENG', value : ' فنی و مهندسی '    },
        {key : 'HWE', value : ' مهندس سخت افزار ' },
        {key : 'SWE', value : ' مهندسی نرم افزار '},
        {key : 'IT' , value : ' مهندسی شبکه '     },
        {key : 'AI' , value : ' هوش مصنوعی '      },
        ],
    }
    return (
        <div>
          <div>
            <SearchSide data = {catdata} UpdateFilter={this.UpdateFilter} />
          </div>
          <div>
            <SearchResult
              Keyword={this.state.Keyword}
              Category={this.state.Category}
              />
          </div>
        </div>
    )
  }

}

export default Search

/*
UpdateGenres    = (Genres) => {
  this.setState({Genres})
}
UpdatePlatforms    = (Platforms) => {
  this.setState({Platforms})
}
UpdateRegions    = (Regions) => {
  this.setState({Regions})
}
<div className="App">
  <br/><br/><br/><br/>
  <p> SEARCH SIDE </p>
  <p> SEARCH RESULTS </p>

  <br /><br />        <br /><br />
</div>
*/
