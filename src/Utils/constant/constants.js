// Author: Sina
const CONSTANTS = {
  BASE_API_URL: 'http://localhost:4848',
  //BASE_API_URL: 'http://94.130.111.197:4848',
  AUTHENTICATION : {
    //SYNC_APP: '/authentication/sync-app',
    SIGN_IN             : 'authentication/register/signin',
    SIGN_UP             : 'authentication/register/signup',
    VERIFY_PHONE_CODE   : 'authentication/register/verify',
    RESEND_CODE         : 'authentication/register/resend',
    CHANGE_PHONE        : 'authentication/register/change',
    CHECK_USER          : 'authentication/register/checkuser',
    CHECK_PHONE         : 'authentication/register/checkphone',
    CHECK_EMAIL         : 'authentication/register/checkemail',
    CHECK_RNN           : 'authentication/register/checkrnn',
    CHECK_LNN           : 'authentication/register/checklnn',
    FINAL               : 'authentication/register/final',
    RESET_PASSWORD      : 'authentication/password/reset',
    VERIFY_RESET_PASS   : 'authentication/password/verify',
    SUBMIT_NEW_PASS     : 'authentication/password/submit',

  },
  SEARCH  : {
    BASIC               : 'search/basic',
    ADVANCE             : 'search/advance',
  },
  PRODUCT : {
    GET_ALL_PRODUCTS_ITEM      :  'product',
    GET_ALL_PRODUCTS           :  'product',
    GET_DETAIL                 :  'product/detail',
    PURCHASE                   :  'product/purchase',
    CARTTOT                    :  'product/carttotal',
    BUYTOTAL                   :  'product/buytotal',
    ADDTOCART                  :  'product/addtocart',
    PAYCART                     : 'product/paycart',
    REMOVEFROMCART             :  'product/removefromcart',
    TOP                        :  'product/top',
    ADD                        :  'product/add',
    DOWNLOAD                   :  'product/download',

  },
  USER : {
    SIGN_OUT            : 'user/signout',
    PROFILE             : 'user/getprofile',
    ADD_BALLANCE        : 'user/ballance/add',

  },
  ERROR_MESSAGE : {
    E_400 : "خطا در ارسال درخواست",
    E_401 : "دسترسی نامعتبر",
    E_403 : "دسترسی غیر مجاز",
    E_404 : {
      ITEM : " ITEM NOT FOUND ",
      USER : "کاربر مورد نظر یافت نشد",
      COMPANY : "کمپانی مورد نظر یافت نشد",
      CATEGORY : "گروه مورد نظر یافت نشد",
    },
    E_406 : "درخواست غیر قابل قبول",
    E_409 : "ایجاد تداخل",
    E_429 : "تعداد درخواست ها زیاد است",
    E_500 : "مشکل ارتباط با سرور لطفا بعدا دوباره تلاش نمایید",
  }
}
export default CONSTANTS
