// Author: Sina

export default class RequestBuilder {
  constructor() {
    this.headers = {}
    this.url = ''
    this.method = 'GET'
    this.body = null
    this.isBinary = false
    this.options = {}
    this.queries = {}
  }

  addQuery (key, value) {
    this.queries = { ...this.queries, [`${key}`]: value }
    return this
  }

  addHeader (key, value) {
    this.headers = { ...this.headers, [`${key}`]: value }
    return this
  }

  setUrl (url) {
    this.url = url
    return this
  }

  setMethod (method) {
    this.method = method
    return this
  }

  setBinary () {
    this.isBinary = true
    return this
  }

  setBody (body) {
    if (this.isBinary)
      this.body = body
    else
      this.body = JSON.stringify(body)
    return this
  }

  addOption (key, value) {
    this.options = { ...this.options, [`${key}`]: value }
    return this
  }

  getRequest () {
    this.addOption('method', this.method)
    this.addOption('headers', this.headers)
    if (this.method !== 'GET')
      this.addOption('body', this.body)

    if (Object.keys(this.queries).length !== 0 && this.queries.constructor === Object) {
      if (this.url.slice(-1) === '/')
        this.url = this.url.slice(0, -1)
      this.url = this.url + '?'
      Object.keys(this.queries).forEach((key) => {
        this.url = this.url + key + "=" + this.queries[key] + "&"
      })
      this.url = this.url.slice(0, -1)
    }
    console.log("REQ :: ", "URL : " ,this.url, "OPTION : ",this.options);
    return new Request(this.url, this.options)
  }
}
