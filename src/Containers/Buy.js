import React, { Component } from 'react';
import {
    Headers,
    Footer,
    ProductCardBuy
  } from '../Components'
import { connect } from 'react-redux'
import { ProductActions } from '../Redux/Actions'
import { Txt } from '../Utils'

  
class Buy extends Component {
    constructor(props){
        super(props);
        this.state = {
            Data : '',
            ProductLoading : true,
            List : '',
          }
    }
    componentDidMount(){
      this.fetchBought()
    }
    fetchBought = ()=>{
      this.props.dispatch(ProductActions.buyTotal()).then(
        function(data){
              const {error,} = data;
              if(error){
                  this.setState({error : true})
              } else {
                  const Data = data.payload.product                  
                  let List = []
                  let KEY = 0;
                  Data.forEach( (product) => {
                    List.push(
                      <ProductCardBuy
                        key= {KEY} 
                        Product = {product.productrecord} 
                        recordId={product.id}
                        />
                    )
                    KEY = KEY + 1
                  })
                  this.setState({List , ProductLoading : false})
              }
            }.bind(this)
          )
    }
    render() {
        return (
            <div>
                <Headers signup = {true} OpenLogin = {this.OpenLogin} />
                <section className="blog section">
                  <div className="container">
                    <div className="main-title section__body">
                        <div className="title">{Txt.BoughtItem}</div>
                    </div>
                    <div className="row">
                      {this.state.List}
                    </div>
                  </div>
                </section>
                <Footer />
            </div>
        );
    }
}

export default connect()(Buy);