import React, { Component } from 'react'
import {
  Responsive,
  Segment,
  Visibility,
} from 'semantic-ui-react'
import { Sidebar, Menu, } from 'semantic-ui-react'
import {
  FixedMenu,
  Headers,
  Footer,
  Administrator
} from '../Components'
import { connect } from 'react-redux'

import {ProfileView,SearchBar } from '../Components'

class Admin extends Component {
  state = { visible: false }

  hideFixedMenu = () => this.setState({ fixed: false })
  showFixedMenu = () => this.setState({ fixed: true })
  toggleVisibility = () => this.setState({ visible: !this.state.visible })

  componentDidUpdate(nextProps){
    if(nextProps.Sidebar !== this.props.Sidebar){
      this.toggleVisibility()
    }
  }
  handleContextRef = contextRef => this.setState({ contextRef })
  render() {
    const { fixed,visible,contextRef } = this.state
    return (
      <div className="">
        <Headers signup = {true} OpenLogin = {this.OpenLogin} />
        <Administrator />
        <Footer />
      </div>
      )
  }
}

const mapStateToProps = (state) => {
  return { Sidebar : state.Toast.sidebar }
}
export default connect(mapStateToProps)(Admin)
