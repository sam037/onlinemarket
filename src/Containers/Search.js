/*
Author : MJS

*/
import React, { Component } from 'react'
import {
  Input,
  Icon,
} from 'semantic-ui-react'
import {
  Headers,
  Footer,
  Search,
} from '../Components'
import { connect } from 'react-redux'
import { Txt } from '../Utils'
//import { Link } from 'react-router-dom'


class SearchContainer extends Component {
  state = { visible: false , keyword : ''}

  componentDidUpdate(nextProps){
  }
  setKeyword = (event) => {
    this.keyword = event.target.value
  }
  _handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      this.setState({keyword : this.keyword})
    }
  }
  handleContextRef = contextRef => this.setState({ contextRef })
  render() {
    const { fixed,visible,contextRef } = this.state
    return (
      <div className="">
        <Headers signup = {true} OpenLogin = {this.OpenLogin} />
        <div style = {{marginRight : '16%' ,marginLeft : '16%',marginTop : '100px'}}>
          <Input
            className = 'rightToleft'
            fluid
            onChange    = {this.setKeyword}
            onKeyPress={this._handleKeyPress}
            />
            <center>
            <button  
              className="btn btn-blueGreen" 
              onClick={() => {this.setState({keyword : this.keyword})} }>
                {Txt.Search}
            </button>
            </center>
          </div>
        <section className="about section">
          <Search keyword = {this.state.keyword} />
        </section>
        <Footer />
      </div>
    )
  }
}
const mapStateToProps = (state) => {
  return { Sidebar : state.Toast.sidebar }
}

export default connect(mapStateToProps)(SearchContainer)
