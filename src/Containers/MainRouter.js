/*
Author : MJS
*/
import React from 'react'
import { Switch, Route} from 'react-router-dom'
import HomePage from './HomePage'
import Search from './Search'
import SignUpContainer from './SignUpContainer'
import {Footer } from '../Components'
import Admin from './Admin'
import Cart from './Cart'
import Buy from './Buy'

class MainRouter extends React.Component {
  render(){
    return (
      <main className="app">
        <Switch>
          <Route path='/' component={ HomePage } exact />
          <Route path='/index' component={ HomePage } exact />
          <Route path='/search' component={ Search } exact />
          <Route path='/Signup' component={SignUpContainer} exact />
          <Route path='/cart' component={Cart} exact />
          <Route path='/buy' component={Buy} exact />
          <Route path='/admin' component={ Admin } exact />
        </Switch>
      </main>
    )
  }
}

export default MainRouter;
