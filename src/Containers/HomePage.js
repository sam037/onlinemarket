import React, { Component } from 'react'
import {
  Responsive,
  Segment,
  Visibility,
} from 'semantic-ui-react'
import { Sidebar, Menu, Sticky } from 'semantic-ui-react'
import {
  AboutServices,
  ProductTop,
  Headers,
  BriefIntroduction,
  Footer
} from '../Components'
import '../dist/styles/styles.css'
import { connect } from 'react-redux'
import Fade from 'react-reveal/Fade';

import {ProfileView,SearchBar } from '../Components'

class HomePage extends Component {
  state = { visible: false }

  hideFixedMenu = () => this.setState({ fixed: false })
  showFixedMenu = () => this.setState({ fixed: true })
  toggleVisibility = () => this.setState({ visible: !this.state.visible })

  componentDidUpdate(nextProps){
    if(nextProps.Sidebar !== this.props.Sidebar){
      this.toggleVisibility()
    }
  }
  handleContextRef = contextRef => this.setState({ contextRef })
  render() {
    const { fixed,visible,contextRef } = this.state
    return (
      <div className="">
        <Headers signup = {true} OpenLogin = {this.OpenLogin} />
        <Fade bottom>
          <BriefIntroduction />
        </Fade>
        <Fade bottom>
            <section className="about section">
                <AboutServices />
            </section>
        </Fade>
        <section className="blog section">
          <ProductTop />
        </section>
        <Footer />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return { Sidebar : state.Toast.sidebar }
}
export default connect(mapStateToProps)(HomePage)
/*
<div>
          <Sidebar.Pushable as={Segment}>
            <Sidebar
              as={Menu}
              animation='overlay'
              width='wide'
              direction='right'
              visible={visible}
              icon='labeled'
              vertical
              inverted
            >
              <Sticky context={contextRef}>
                <Menu.Item name='home'>
                  <ProfileView />
                </Menu.Item>
              </Sticky>
            </Sidebar>
            <Sidebar.Pusher>
              
            </Sidebar.Pusher>
          </Sidebar.Pushable>
        </div>
      </Responsive>

*/