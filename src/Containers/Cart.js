import React, { Component } from 'react';
import {
    Headers,
    Footer,
    ProductCardCart
  } from '../Components'
import { connect } from 'react-redux'
import { ProductActions } from '../Redux/Actions'
import { Txt } from '../Utils'
import { Redirect } from 'react-router-dom'

class Cart extends Component {
  constructor(props){
      super(props);
      this.state = {
          Data : '',
          ProductLoading : true,
          List : '',
          ToBuyList : false,
        }
  }
  payCart = () => {
    this.props.dispatch(ProductActions.payCart()).then(
      (result) =>{
        this.setState({ToBuyList : true })
      }
    )
  }
  fetchCart = ()=>{
    this.props.dispatch(ProductActions.cartTotal()).then(
      function(data){
            const {error,} = data;
            if(error){
                this.setState({error : true})
            } else {
                const Data = data.payload.product                  
                let List = []
                let KEY = 0;
                Data.forEach( (product) => {
                  List.push(
                    <ProductCardCart 
                      key= {KEY} 
                      Product = {product.productrecord} 
                      recordId={product.id}
                      fetchCart = {this.fetchCart}
                      />
                  )
                  KEY = KEY + 1
                })
                this.setState({List , ProductLoading : false})
            }
          }.bind(this)
        )
  }
  componentDidMount(){
    this.fetchCart()
  }
  render() {
    if(this.state.ToBuyList){
      return(
        <Redirect to='/buy' />
      )
    } else {
      return (
        <div>
            <Headers signup = {true} OpenLogin = {this.OpenLogin} />
            <section className="blog section">
              <div className="container">
                <div className="main-title section__body">
                    <div className="title">{Txt.Cart}</div>
                </div>
                <div className="row">
                  {this.state.List}
                </div>
              </div>
            </section>
            <center>
              <div class="col-xs-2 col-sm-4 hide-xs">
                  <button  class="btn btn-blueGreen" onClick={this.payCart}>
                    {Txt.SubmitCart}
                  </button>
              </div>
            </center>
            <Footer />
        </div>
      );
    }
  }
}

export default connect()(Cart);