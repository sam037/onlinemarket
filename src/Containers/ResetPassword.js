/*
Author : MJS
props :
*/
import React, { Component,} from 'react';
//import { connect } from 'react-redux'
import {
    Headers,
    SubmitResetPassword,
    Footer} from '../Components'
//import Reveal from 'react-reveal/Reveal';
import '../dist/styles/styles.css'
import {Helmet} from "react-helmet";

class HomePage extends Component {
    constructor(props){
        super(props)
        this.state = {

        }
    }
    render() {
        console.log(this.props.match.params.resetcode)
        return (
            <main className="app">
                <Helmet>
                    <meta charSet="utf-8" />
                    <title> بازیابی گذر واژه </title>
                </Helmet>
                <Headers />
                <section className="login">
                    <SubmitResetPassword resetcode = {this.props.match.params.resetcode} />
                </section>
                <Footer />
            </main>
    );
  }
}
  
export default HomePage
