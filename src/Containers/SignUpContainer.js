// Authors =  {logic : MJ SAM }
import _ from 'lodash'
import React, { Component } from 'react'
import { SignupComponent ,VerifyComponent ,Final ,Headers} from '../Components'
import '../dist/styles/styles.css'
import { AuthActions , } from '../Redux/Actions'
//import {Helmet} from "react-helmet";
import {connect , } from 'react-redux'
import { withRouter} from 'react-router'
import Fade from 'react-reveal/Fade';

//import './Authentication.css'
class SignUp extends Component {
  constructor(props){
    super(props)
    if(_.has(this.props.match.params, 'refferal')){
        this.props.dispatch(AuthActions.SetRefferal(this.props.match.params.refferal))
    }
    this.state = {
      Screen : 'Pre',
      UserInfo : '',
      Phone : '',
    }
  }
  ChangeScreen = (Screen) => this.setState({Screen})
  updateUserInfo = (newUsrInfo,callback,Phone) => {
      this.setState({UserInfo : newUsrInfo , Phone : Phone},callback)
  }
  render() {
    let CurrentComponent = null
    switch (this.state.Screen) {
      case 'Pre'  :
        CurrentComponent = (
            <SignupComponent
              push = {this.props.history.push}
              ChangeScreen = {this.ChangeScreen}
              updateUserInfo = {this.updateUserInfo}
              UserInfo = {this.state.UserInfo}
              />
        )
        break;
      case 'Verify':
        CurrentComponent = (
            <VerifyComponent
              ChangeScreen = {this.ChangeScreen}
              updateUserInfo = {this.updateUserInfo}
              Phone = {this.state.Phone}
              UserInfo = {this.state.UserInfo}
              />
        )
        break;
      case 'Final':
        CurrentComponent =  (
            <Final
              ChangeScreen = {this.ChangeScreen}
              UserInfo = {this.state.UserInfo}
             />
        )
        break;
        
      default:
        CurrentComponent = (
          <SignupComponent
            ChangeScreen = {this.ChangeScreen}
            updateUserInfo = {this.updateUserInfo}
            />
        )
      }
    return(
    <main className="app">
        <Headers signup = {false} />
        <section className="login">
          <Fade top>
            {CurrentComponent}
          </Fade>
        </section>
    </main>
    )
  }
}

export default withRouter(connect()(SignUp))
/*
<Helmet>
            <meta charSet="utf-8" />
            <meta name="description" content=""/>
            <meta name="keywords" content=""/>
            <meta name="title" content="بنیان‌گذاران"/>
            <title> پیوستن به آرتام </title>
</Helmet>
*/