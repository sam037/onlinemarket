/*
Author : MJS
props :
*/
import React, { Component } from 'react';
import { UserProfileHeader , } from '../Components'
import { connect } from 'react-redux'
import { UserActions, AuthActions, ToastActions} from '../Redux/Actions'
import { Txt } from '../Utils'
import { Redirect } from 'react-router'
import "react-tabs/style/react-tabs.css";
import {
    Founder
} from '../Components'
import '../dist/styles/styles.css'

class LoginContainer extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            isLoading           : false,

            LoginNotValid       : false,

            User                : '',
            UserInValid         : false,
            UserLengthInValid   : false,
            UserCharInValid     : false,
            UserStartInValid    : false,

            Pass                : '',
            PassInValid         : false,
            PassLengthInValid   : false,
            PassCharInValid     : false,
            PassStartInValid    : false,
        }
  }
  componentDidMount(){
  }
  UserOnChange = (event) => {
    let user = event.target.value.trim()
    var reLength = /^[ A-Za-z0-9_@.#&+-]{4,24}$/i;
    var reChar = /^[A-Za-z0-9_.]*$/i;
    var reStart = /^[a-zA-z][A-Za-z0-9_@.#&+-]*$/i;
    var reLengthValid = reLength.test(user)
    var reCharValid = reChar.test(user)
    var reStartValid = reStart.test(user)
    
    console.log(reLengthValid)
    console.log(reCharValid)
    console.log(reStartValid)
    
    if(reLengthValid && reCharValid && reStartValid){
        this.setState({
            User                : user,
            UserLengthInValid   : !reLengthValid ,
            UserCharInValid     : !reCharValid ,
            UserStartInValid    : !reStartValid,
        })
    } else {
        this.setState({
            UserLengthInValid   : !reLengthValid ,
            UserCharInValid     : !reCharValid ,
            UserStartInValid    : !reStartValid,
        })
    }
  }
  PassOnChange = (event) => {
    let pass = event.target.value.trim()
    var reLength = /^[ A-Za-z0-9_@.#&+-]{4,24}$/i;
    var reChar = /^[A-Za-z0-9_.]*$/i;
    var reStart = /^[a-zA-z][A-Za-z0-9_@.#&+-]*$/i;
    var reLengthValid = reLength.test(pass)
    var reCharValid = reChar.test(pass)
    var reStartValid = reStart.test(pass)
    let RepPassInValid = (this.state.RepPass == pass)
    if(reLengthValid && reCharValid && reStartValid){
        this.setState({
            Pass                : pass,
            RepPassInValid      : RepPassInValid,
            PassLengthInValid   : !reLengthValid,
            PassCharInValid     : !reCharValid,
            PassStartInValid    : !reStartValid,
            })
    } else {
        this.setState({
            RepPassInValid      : RepPassInValid,
            PassLengthInValid   : !reLengthValid,
            PassCharInValid     : !reCharValid,
            PassStartInValid    : !reStartValid,
            })
    }
  }
  AskForLogin = () => {
    const { dispatch } = this.props
    const { Pass ,User } = this.state
    this.setState({isLoading : true})
    if(Pass !== '' && User !== ''){
      dispatch(AuthActions.signIn(User,"username",Pass)).then(
        function(data){
              const {error,payload} = data;
              if(error){
                  //this.setState({
                  //    isLoading : false,
                  //    LoginNotValid :   true,
                //})
              } else {
                  //console.log("PAY ",payload)
                  //this.setState({proceed : true})
              }
            }.bind(this)
          )
      } else {
        //dispatch(ToastActions.ShowToast(Txt.FillUserPass))
        //this.setState({isLoading : false})
      }
    }

  render() {
    console.log(this.state.User,this.state.Pass)
    if(this.props.UsrID){
        return(
            <div> {window.location.assign("/")} </div>
        )
    }
    return (
<div>
    <main className="app">
        <header className="main-header static header">
            <div className="container-fluid">
                <div className="header--layout row">
                <div className="col-xs-10 col-sm-3"><a href="/" className="logo">
                    <img src="dist/images/svg/logo.svg"/></a>
                </div>
                <div className="col-xs-2 col-sm-6 align-center">
                    <nav className="header--menu"><a href="about.html" className="current">درباره ما</a><a href="founders.html">بنیان‌گذاران</a><a href="profile.html">پروفایل کاربری</a><a href="login.html" className="show-xs">به ما بپیوندید</a></nav>
                    <div className="burger-container">
                    <div id="burger">
                        <div className="bar topBar"></div>
                        <div className="bar btmBar"></div>
                    </div>
                    </div>
                </div>
                </div>
            </div>
        </header>
        <section className="login">
            <div className="container">
            <div className="form-box">
                <h3>ورود</h3>
                <label className="label">
                <input 
                    type="text" 
                    onChange = {this.UserOnChange}
                    required="required" 
                    placeholder="تلفن‌همراه (شناسه کاربری)" 
                    className="input"/>
                    {this.state.UserLengthInValid && 
                        <span className="validate-error"> {Txt.UserLength}</span>}
                    {this.state.UserCharInValid && 
                        <span className="validate-error"> {Txt.UserChar}</span>}
                    {this.state.UserStartInValid && 
                        <span className="validate-error"> {Txt.UserStart}</span>}
                </label>
                <label className="label">
                    <input 
                        type="password" 
                        onChange = {this.PassOnChange}
                        required="required" 
                        placeholder="رمز عبور" 
                        className="input"/>
                    {this.state.PassLengthInValid && 
                        <span className="validate-error"> {Txt.PassLength}</span>}
                    {this.state.PassCharInValid && 
                        <span className="validate-error"> {Txt.PassChar}</span>}
                    {this.state.PassStartInValid && 
                        <span className="validate-error"> {Txt.PassStart}</span>}
                </label>
                <div className="control">
                    <input 
                        id="reminder" 
                        type="checkbox" 
                        name="reminder" 
                        className="control__input blue"/>
                <label htmlFor="reminder" className="control__label">مرا به خاطر بسپار</label>
                </div>
                <label className="label">
                {this.state.LoginNotValid && <span className="validate-error"> {Txt.UserNotValid}</span>}
                <button className="green-btn btn" onClick ={this.AskForLogin}>
                    ورود
                </button>
                </label>
                <label className="label">
                <div className="notice">آیا رمز عبور خودرا فراموش کردید؟<a href="#">بازیابی رمز عبور</a></div>
                </label>
                <label className="label">
                <div className="notice">قبلا در آرتام ثبت‌نام نکرده‌اید؟<a href="register.html">ثبت‌نام در آرتام</a></div>
                </label>
            </div>
            </div>
        { this.state.isLoading &&
        <div className="loader-comp">
            <div className="loader">
                <div className="spinner yellow"></div>
                <div className="spinner orange"></div>
                <div className="spinner red"></div>
                <div className="spinner pink"></div>
                <div className="spinner violet"></div>
                <div className="spinner mauve"></div>
                <div className="spinner light-yellow"></div>
            </div>
        </div>}
    </section>
    </main>
</div>
    );
  }
}

const mapStateToProps = (state) => {
    return {  UsrID: state.Authentication.user_id,
              first_name: state.User.profile.first_name,
              last_name: state.User.profile.last_name,
              username: state.User.profile.username,
              phone: state.User.profile.phone,
    };
  };
  
  export default connect(mapStateToProps)(LoginContainer)