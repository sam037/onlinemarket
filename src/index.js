/*
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();

*/
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import { configureStore ,loadState , saveState } from './Redux/config'
import { BrowserRouter } from 'react-router-dom'
import registerServiceWorker from './registerServiceWorker'
import MainRouter from './Containers/MainRouter'
import './MainStyle.css'
//import 'react-quill/dist/quill.snow.css';

let { store,persistor } = configureStore()

store.subscribe(()=>{
  saveState(store.getState())
  console.log("saved")
})

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
        <MainRouter/>
    </BrowserRouter>
  </Provider>
,
  document.getElementById('root')
)

registerServiceWorker()
